<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="users"/>

<html>
<head>
    <title>List of users</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of users</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="user.email"/></th>
        <th><fmt:message key="user.name"/></th>
        <th><fmt:message key="user.surname"/></th>
        <th><fmt:message key="user.address"/></th>
        <th><fmt:message key="user.phoneNumber"/></th>
        <th><fmt:message key="user.type"/></th>
        <th><fmt:message key="user.specialization"/></th>
    </tr>
    </thead>
    <c:if test="${sessionScope.user.isAdmin()}">
        <c:forEach items="${requestScope.users}" var="user">
            <tr>
                <td><c:out value="${user.getEmail()}"/></td>
                <td><c:out value="${user.getName()}"/></td>
                <td><c:out value="${user.getSurname()}"/></td>
                <td><c:out value="${user.getAddress()}"/></td>
                <td><c:out value="${user.getPhoneNum()}"/></td>
                <td><c:out value="${user.getType()}"/></td>
                <c:choose>
                    <c:when test="${user.getSpecialization() != null}">
                        <td><c:out value="${user.getSpecialization()}"/></td>
                    </c:when>
                    <c:otherwise>
                        <td>not a teacher</td>
                    </c:otherwise>
                </c:choose>
                <form action="deleteUser" method="post">
                    <td>
                        <button type="submit" class="btn btn-secondary" name="deleteUserId"
                                value="${user.getUserId()}">Delete
                        </button>
                    </td>
                </form>
                <td/>
            </tr>
            <form action="updateUser" method="post">
                <tr>
                    <td>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control"
                                   placeholder="email" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="name" pattern="[\w*\S]*" title="Only letters"
                                   class="form-control" placeholder="name" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="surname" pattern="[\w*\S]*" title="Only letters"
                                   class="form-control" placeholder="surname" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="address" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$"
                                   title="Only letters and commas" class="form-control"
                                   placeholder="address" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="phoneNum"
                                   pattern='\s{0,}\+{1,1}375\s{0,}\({0,1}(([2]{1}([5]{1}|[9]{1}))|([3]{1}[3]{1})|([4]{1}[4]{1}))\)\s{0,}[0-9]{3,3}\s{0,}[0-9]{4,4}'
                                   title="enter number in this format: +375(29)7001010" class="form-control"
                                   placeholder="phone number" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select name="userType" class="form-control">
                                <option>TEACHER</option>
                                <option>STUDENT</option>
                            </select>
                        </div>
                    </td>
                    <td><input type="hidden" name="updateUserId" value="${user.getUserId()}">
                        <c:if test="${user.getType().name().equals('TEACHER')}">
                            <div class="form-group">
                                <input type="text" name="specialization" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$"
                                       title="Only letters and commas" class="form-control" required>
                            </div>
                        </c:if>
                    </td>
                    <td><input type="submit" class="btn btn-secondary" value="Update"></td>
                    <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                </tr>
            </form>
        </c:forEach>
    </c:if>
    <div class="form-group">
        <p>All fields for update are required</p>
    </div>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>