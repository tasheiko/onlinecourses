<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=utf-8" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Registration Form</title>
</head>
<body>
<div>
    <h3>Student Registration Form</h3>
</div>
<form action="courses/registration" method="post">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>User email *</label>
                    <input type="text" class="form-control" name="email" maxlength="30"
                           pattern="^[^\s]+[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Enter email" required>
                    <small class="form-text text-muted">(max 30 characters a-z, A-Z, 0-9)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Password *</label>
                    <input type="password" class="form-control" name="password" maxlength="30"
                           pattern="^[^\s]*[\w]{6,}$" title="Six or more characters" placeholder="Enter password"
                           required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>User name *</label>
                    <input type="text" pattern="[\w*\S]*" title="Only letters"
                           class="form-control" name="name" maxlength="30" placeholder="Enter your name" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>User surname *</label>
                    <input type="text" pattern="[\w*\S]*" title="Only letters"
                           class="form-control" name="surname" maxlength="30"
                           placeholder="Enter your surname" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Address *</label>
                    <input type="text" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$"
                           title="Only letters and commas with spaces"
                           class="form-control" name="address" maxlength="60"
                           placeholder="Enter address" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>User phone number *</label>
                    <input type="text" class="form-control" name="phoneNum" maxlength="30"
                           pattern="\s{0}\+{1,1}375\s{0,}\({0,1}(([2]{1}([5]{1}|[9]{1}))|([3]{1}[3]{1})|([4]{1}[4]{1}))\)\s{0,}[0-9]{3,3}\s{0,}[0-9]{4,4}"
                           placeholder="Enter phone number" required>
                    <small class="form-text text-muted">(for example +375(29)7001010)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Teacher or student *</label>
                    <select name="userType" class="form-control">
                        <option>TEACHER</option>
                        <option>STUDENT</option>
                    </select>
                    <small class="form-text text-muted">(choose one)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Specialization(for teachers)</label>
                    <input type="text" class="form-control" name="specialization" maxlength="200"
                           pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$" placeholder="Enter specialization">
                    <small class="form-text text-muted">(max 200 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="btn btn-secondary" value="Submit">
                <input type="reset" class="btn btn-secondary" value="Clear fields">
            </td>
        </tr>
        <div class="form-group">
            <p>All fields marked with * are required</p>
        </div>
    </table>
</form>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href="/onlineCourses" class="alert-link">click here</a>
</div>
</body>
</html>