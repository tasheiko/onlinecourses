<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Server error page</title>
</head>
<body>
<c:set var="invalidField" value="${requestScope.invalidField}"/>
<c:set var="errorCode" value="${requestScope.errorCode}"/>
<c:if test="${invalidField != null}">
    <div class="alert alert-warning" role="alert">
        <strong>Server error, ${errorCode} - ${invalidField}</strong><br/>
    </div>
</c:if>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>
