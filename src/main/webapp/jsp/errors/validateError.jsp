<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Validation error Page</title>
</head>
<body>
<c:set var="invalidField" value="${requestScope.invalidField}"/>
<div class="alert alert-warning" role="alert">
    <strong>${invalidField}</strong><br/>
</div>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>