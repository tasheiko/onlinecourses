<%@ include file="../general/bootstrapJs.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page</title>
</head>
<body>
<div>
    <c:choose>
        <c:when test="${requestScope.loginError != null}">
            <h1>Your Login Was Unsuccessful - Please Try Again</h1>
        </c:when>
        <c:otherwise>
            <h1>You do not have permission to access this resource</h1>
        </c:otherwise>
    </c:choose>
    <div class="alert alert-warning" role="alert">
        Come back to login <a href="/onlineCourses" class="alert-link">click here</a>
    </div>
</div>
</body>
</html>