<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="bootstrapJs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Login Page</title>
</head>
<body>
<div>
    <h2>Login Page</h2>
</div>
<form method="post" action="/onlineCourses/courses/login">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>User email(login):</label>
                    <input type="email" name="userEmail" class="form-control" title="Email"
                           maxlength="50" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="userPassword" title="Six or more characters"
                           pattern="^[^\s]*[\w]{6,}$" maxlength="50" required>
                </div>
            </td>
        </tr>
        <tr>
            <td><input type="submit" class="btn btn-lg btn-secondary btn-block text-uppercase" value="Login"/></td>
        </tr>
    </table>
</form>
<div class="alert alert-secondary" role="alert" align="center">
    New User? <a href="registration" class="alert-link">Register Here</a>
</div>
</body>
</html>