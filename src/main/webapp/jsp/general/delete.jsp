<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=utf-8" %>

<html>
<head>
    <title>Deletion list</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">Deletion list</h2>
</div>
<c:set var="deletedCourse" value="${requestScope.courseDel}"/>
<c:set var="deletedTask" value="${requestScope.taskDel}"/>
<c:set var="deletedMark" value="${requestScope.markDel}"/>
<c:set var="deletedUser" value="${requestScope.userDel}"/>
<c:choose>
    <c:when test="${deletedCourse != null}">
        <div class="alert alert-success" role="alert">
            <li>Course <c:out value="${deletedCourse.getType()}"/> was deleted from DB</li>
        </div>
    </c:when>
    <c:when test="${deletedTask != null}">
        <div class="alert alert-success" role="alert">
            <li>Task <c:out value="${deletedTask.getNameOfTask()}"/> was deleted from DB</li>
        </div>
    </c:when>
    <c:when test="${deletedMark != null}">
        <div class="alert alert-success" role="alert">
            <li>Mark <c:out value="${deletedMark.getMark()}"/> with comment:"<c:out
                    value="${deletedMark.getComment()}"/>" was deleted from DB
            </li>
        </div>
    </c:when>
    <c:when test="${deletedUser != null}">
        <div class="alert alert-success" role="alert">
            <li>User <c:out value="${deletedUser.getName()}"/> <c:out value="${deletedUser.getSurname()}"/> was deleted
                from DB
            </li>
        </div>
    </c:when>
</c:choose>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>