<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="courses.entities.UserType" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=utf-8" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="menu"/>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Menu</title>
</head>

<body>
<div class="jumbotron">
    <h1>List of online courses</h1>
    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <article>
                <h4>Welcome ${sessionScope.user.getEmail()}!</h4>
            </article>
            <div class="alert alert-primary" role="alert">
                <a href="/onlineCourses/courses/logout" class="alert-link">Logout</a>
            </div>
        </c:when>
        <c:otherwise>
            You can't watch this form before you login.
            <div class="alert alert-danger" role="alert">
                To login <a href="/onlineCourses" class="alert-link">Click here</a>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<c:if test="${sessionScope.user != null}">
    <c:choose>
        <c:when test="${UserType.STUDENT.toString().equalsIgnoreCase(sessionScope.user.getType().name())}">
            <ul>
                <div class="list-group">
                    <li><a href="/onlineCourses/courses/course"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="course.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/task"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="task.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/mark"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="mark.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/teacher"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="teacher.list"/></a></li>
                </div>
            </ul>
        </c:when>
        <c:when test="${UserType.TEACHER.toString().equalsIgnoreCase(sessionScope.user.getType().name())}">
            <ul>
                <div class="list-group">
                    <li><a href="/onlineCourses/courses/course"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="course.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/student"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="student.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/task"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="task.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/mark"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="mark.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/teacher"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="teacher.list"/></a></li>
                </div>
            </ul>
        </c:when>
        <c:when test="${sessionScope.user.isAdmin()}">
            <ul>
                <div class="list-group">
                    <li><a href="/onlineCourses/courses/course"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="course.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/student"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="student.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/task"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="task.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/mark"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="mark.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/user"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="user.list"/></a></li>
                    <br/>
                    <li><a href="/onlineCourses/courses/teacher"
                           class="list-group-item list-group-item-action list-group-item-dark"><fmt:message
                            key="teacher.list"/></a></li>
                </div>
            </ul>
        </c:when>
    </c:choose>
    <div class="footer-copyright text-center py-3">Copyright Yan Tasheika
    </div>
</c:if>
</body>
</html>