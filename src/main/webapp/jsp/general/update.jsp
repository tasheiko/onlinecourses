<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=utf-8" %>

<html>
<head>
    <title>Updating list</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">Update list</h2>
</div>
<c:choose>
    <c:when test="${requestScope.updatedCourse != null}">
        <div class="alert alert-success" role="alert">
            Course at number ${requestScope.updatedCourse.getCourseId()} was updated in DB with following details:
        </div>
        <table class="table table-dark">
            <thead>
            <tr>
                <th>Type</th>
                <th>Title</th>
                <th>Number of hours</th>
            </tr>
            </thead>
            <tr>
                <td>${requestScope.updatedCourse.getType()}</td>
                <td>${requestScope.updatedCourse.getTitle()}</td>
                <td>${requestScope.updatedCourse.getNumOfHours()}</td>
            </tr>
        </table>
    </c:when>
    <c:when test="${requestScope.updatedTask != null}">
        <div class="alert alert-success" role="alert">
            Task at number ${requestScope.updatedTask.getTaskId()} was updated in DB with following details:
        </div>
        <table class="table table-dark">
            <thead>
            <tr>
                <th>Name of task</th>
                <th>Description</th>
            </tr>
            </thead>
            <tr>
                <td>${requestScope.updatedTask.getNameOfTask()}</td>
                <td>${requestScope.updatedTask.getDescription()}</td>
            </tr>
        </table>
    </c:when>
    <c:when test="${requestScope.updatedMark != null}">
        <div class="alert alert-success" role="alert">
            Mark at number ${requestScope.updatedMark.getMarkId()} was updated in DB with following details:
        </div>
        <table class="table table-dark">
            <thead>
            <tr>
                <th>Comment</th>
                <th>Mark</th>
            </tr>
            </thead>
            <tr>
                <td>${requestScope.updatedMark.getComment()}</td>
                <td>${requestScope.updatedMark.getMark()}</td>
            </tr>
        </table>
    </c:when>
    <c:when test="${requestScope.updatedUser != null}">
        <div class="alert alert-success" role="alert">
            User at number: ${requestScope.updatedUser.getUserId()} was updated in the DB with following details:
        </div>
        <table class="table table-dark">
            <thead>
            <tr>
                <th>Email</th>
                <th>Name</th>
                <th>Surname</th>
            </tr>
            </thead>
            <tr>
                <td>${requestScope.updatedUser.getEmail()}</td>
                <td>${requestScope.updatedUser.getName()}</td>
                <td>${requestScope.updatedUser.getSurname()}</td>
            </tr>
        </table>
    </c:when>
</c:choose>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>