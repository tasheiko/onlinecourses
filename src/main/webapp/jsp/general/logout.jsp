<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>logout Page</title>
</head>
<body>
<div>
    <h1>You have successfully logged out</h1>
    <div class="alert alert-success" role="alert">
        To login again <a href="/onlineCourses" class="alert-link">click here</a>
    </div>
</div>
</body>
</html>