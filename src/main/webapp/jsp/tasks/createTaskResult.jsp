<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Tasks changing result</title>
</head>
<body>
<div>
    <c:choose>
        <c:when test="${requestScope.changeTask}">
            <h1>Task was registered</h1>
            <div class="alert alert-secondary" role="alert">
                comeback to menu <a href="/onlineCourses/courses/menu">click here</a>
            </div>
        </c:when>
        <c:otherwise>
            <h1>Task was not created</h1>
            <div class="alert alert-secondary" role="alert">
                comeback to menu <a href="/onlineCourses/courses/menu">click here</a>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>