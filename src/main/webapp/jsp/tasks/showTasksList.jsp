<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="tasks"/>

<html>
<head>
    <title>List of tasks</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of tasks</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="course.title"/></th>
        <th><fmt:message key="task.nameOfTask"/></th>
        <th><fmt:message key="task.description"/></th>
    </tr>
    </thead>
    <c:if test="${sessionScope.user.isAdmin()}">
        <div class="alert alert-success" role="alert">
            <p>All fields for update are required</p>
        </div>
    </c:if>
    <c:forEach items="${requestScope.tasks}" var="task">
        <tr>
        <td><c:out value="${task.getTitle()}"/></td>
        <td><c:out value="${task.getNameOfTask()}"/></td>
        <td><c:out value="${task.getDescription()}"/></td>
        <c:choose>
            <c:when test="${sessionScope.user.isAdmin()}">
                <form action="deleteTask" method="post">
                    <td>
                        <button type="submit" class="btn btn-secondary" name="deleteTaskId"
                                value="${task.getTaskId()}">Delete
                        </button>
                    </td>
                </form>
                <td/>
                </tr>
                <form action="updateTask" method="post">
                    <tr>
                        <td/>
                        <td>
                            <div class="form-group">
                                <input type="text" name="nameOfTask" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z\s.,#+]+[^\s]$"
                                       title="Only letters" class="form-control"
                                       placeholder="name of task" required>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="description" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,#+]+[^\s]$"
                                       title="Only letters and commas" class="form-control"
                                       placeholder="description" required>
                            </div>
                        </td>
                        <td><input type="hidden" name='updateTaskId' value="${task.getTaskId()}">
                            <input type="submit" class="btn btn-secondary" value="Update"></td>
                        <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                    </tr>
                </form>
            </c:when>
            <c:when test="${'teacher'.equalsIgnoreCase(sessionScope.user.getType().name()) && (param.studentId != null)}">
                <form action='/onlineCourses/courses/createMarks' method="post">
                    <td><input type="hidden" name="studentId" value="${param.studentId}"/>
                        <input type="hidden" name="taskId" value="${task.getTaskId()}"/>
                        <input type="submit" class="btn btn-secondary" value="Choose Task"></td>
                </form>
                </tr>
            </c:when>
            <c:otherwise>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</table>
<div class="alert alert-success" role="alert">
    All completed tasks you must send to the teacher's email. You can find it on the <a
        href='/onlineCourses/courses/teacher' class="alert-link">teachers page</a>.
</div>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>