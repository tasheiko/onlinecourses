<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="students"/>

<html>
<head>
    <title>List of students</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of students</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="student.name"/></th>
        <th><fmt:message key="student.surname"/></th>
        <th><fmt:message key="student.address"/></th>
        <th><fmt:message key="student.email"/></th>
        <th><fmt:message key="student.phoneNumber"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.students}" var="student">
        <tr>
            <td><c:out value="${student.getName()}"/></td>
            <td><c:out value="${student.getSurname()}"/></td>
            <td><c:out value="${student.getAddress()}"/></td>
            <td><c:out value="${student.getEmail()}"/></td>
            <td><c:out value="${student.getPhoneNum()}"/></td>
            <c:choose>
                <c:when test="${'teacher'.equalsIgnoreCase(sessionScope.user.getType().name())}">
                    <form action="/onlineCourses/courses/task" method="post">
                        <td>
                            <button type="submit" class="btn btn-success" name="studentId"
                                    value="${student.getUserId()}">Rate student
                            </button>
                        </td>
                    </form>
                </c:when>
            </c:choose>
        </tr>
    </c:forEach>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>