<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mytag" uri="/WEB-INF/mytaglib.tld" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="courses"/>

<html>
<head>
    <title>List of courses</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of courses</h2>
</div>
<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="course.type"/></th>
        <th><fmt:message key="course.title"/></th>
        <th><fmt:message key="course.numOfHours"/></th>
        <th><fmt:message key="course.description"/></th>
        <th><fmt:message key="teacher.name"/></th>
        <th><fmt:message key="teacher.surname"/></th>
        <th><fmt:message key="course.mark"/></th>
    </tr>
    </thead>
    <c:if test="${sessionScope.user.isAdmin()}">
        <div class="alert alert-success" role="alert">
            <p>All fields for update are required</p>
        </div>
    </c:if>
    <c:forEach items="${requestScope.entitiesList}" var="course">
        <tr>
        <td><c:out value="${course.getType()}"/></td>
        <td><c:out value="${course.getTitle()}"/></td>
        <td><c:out value="${course.getNumOfHours()}"/></td>
        <td><c:out value="${course.getDescription()}"/></td>
        <td><c:out value="${course.getName()}"/></td>
        <td><c:out value="${course.getSurname()}"/></td>
        <td><mytag:getAverageMark courseId="${course.getCourseId()}"/></td>
        <c:choose>
            <c:when test="${sessionScope.user.isAdmin()}">
                <form action="deleteCourse" method="post">
                    <td>
                        <button type="submit" class="btn btn-secondary" name="deleteCourseId"
                                value="${course.getCourseId()}">
                            Delete
                        </button>
                    </td>
                </form>
                <form action="createTasks" method="post">
                    <td>
                        <button type="submit" class="btn btn-primary" name="courseId"
                                value="${course.getCourseId()}">Create
                            Task
                        </button>
                    </td>
                </form>
                </tr>
                <form action="updateCourse" method="post">
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="text" name="type" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z\s]+[^\s]$"
                                       title="Only letters and commas" class="form-control"
                                       placeholder="type" required>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="title" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z\s.,#+]+[^\s]$"
                                       title="Only letters and commas" class="form-control"
                                       placeholder="title" required>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="numOfHours" pattern="^[0-9][0-9]?[0-9]?$|^1000$"
                                       title="Only digits" class="form-control"
                                       placeholder="number of hours" required>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="description" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$"
                                       title="Only letters and commas" class="form-control"
                                       placeholder="description" required>
                            </div>
                        </td>
                        <td/>
                        <td><input type="hidden" name="updateCourseId" value="${course.getCourseId()}"></td>
                        <td><input type="hidden" name="updateTeacherId" value="${course.getUserId()}"></td>
                        <td><input type="submit" class="btn btn-secondary" value="Update"></td>
                        <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                    </tr>
                </form>
            </c:when>
            <c:otherwise>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href="menu" class="alert-link">click here</a>
</div>
</body>
</html>