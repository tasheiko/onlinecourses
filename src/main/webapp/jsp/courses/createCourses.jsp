<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<%@ page contentType="text/html;charset=utf-8" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Courses changing</title>
</head>
<body>
<div class="jumbotron">
    <h3>Course change form</h3>
</div>
<form action="changeCourse" method="post">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>Course type</label>
                    <input type="text" class="form-control" name="type" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z\s]+[^\s]$"
                           title="Only letters and commas" placeholder="Enter course type" required>
                    <small class="form-text text-muted">(max 30 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Course title</label>
                    <input type="text" class="form-control" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z\s.,#+]+[^\s]$"
                           title="Only letters and commas" name="title" placeholder="Enter course title" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Number of hours</label>
                    <input type="text" class="form-control" pattern="^[0-9][0-9]?[0-9]?$|^1000$" title="Only digits"
                           name="numOfHours" placeholder="Enter number of hours" required>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" pattern="^[^\s]+[а-яА-ЯёЁa-zA-Z0-9\s.,]+[^\s]$"
                           title="Only letters and commas" name="description" placeholder="Enter description" required>
                    <small class="form-text text-muted">(max 250 characters)</small>
                </div>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="teacherId"
                   value="${param.teacherId}"/>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="btn btn-secondary" value="Submit">
                <input type="reset" class="btn btn-secondary" value="Clear fields">
            </td>
        </tr>
        <div class="form-group">
            <p>All fields are required</p>
        </div>
    </table>
</form>
<div class="alert alert-secondary" role="alert" align="center">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>