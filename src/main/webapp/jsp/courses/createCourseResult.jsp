<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Courses changing result</title>
</head>
<body>
<div>
    <c:choose>
        <c:when test="${requestScope.changeCourse}">
            <h1>Course was created</h1>
            <div class="alert alert-secondary" role="alert">
                comeback to menu <a href="menu">click here</a>
            </div>
        </c:when>
        <c:otherwise>
            <h1>Course was not created</h1>
            <div class="alert alert-secondary" role="alert">
                comeback to menu <a href="menu">click here</a>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<jsp:include page="../general/bootstrapJs.jsp"/>
</body>
</html>