<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="teachers"/>

<html>
<head>
    <title>List of teachers</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of teachers</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="teacher.name"/></th>
        <th><fmt:message key="teacher.surname"/></th>
        <th><fmt:message key="teacher.address"/></th>
        <th><fmt:message key="teacher.email"/></th>
        <th><fmt:message key="teacher.phoneNumber"/></th>
        <th><fmt:message key="teacher.specialization"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.teachers}" var="teacher">
        <tr>
            <td><c:out value="${teacher.getName()}"/></td>
            <td><c:out value="${teacher.getSurname()}"/></td>
            <td><c:out value="${teacher.getAddress()}"/></td>
            <td><c:out value="${teacher.getEmail()}"/></td>
            <td><c:out value="${teacher.getPhoneNum()}"/></td>
            <td><c:out value="${teacher.getSpecialization()}"/></td>
            <c:choose>
                <c:when test="${sessionScope.user.isAdmin()}">
                    <form action="/onlineCourses/courses/createCourses" method="post">
                        <td>
                            <button type="submit" class="btn btn-success" name="teacherId"
                                    value="${teacher.getUserId()}">Create
                                course
                            </button>
                        </td>
                    </form>
                </c:when>
            </c:choose>
        </tr>
    </c:forEach>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>