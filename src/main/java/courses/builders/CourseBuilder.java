package courses.builders;

import static courses.validation.Validator.validateXss;
import static java.lang.Integer.parseInt;

import courses.entities.Course;
import courses.validation.ValidateException;

import javax.servlet.http.HttpServletRequest;

public class CourseBuilder {

    public static Course build(HttpServletRequest req) throws ValidateException {
        String type = validateXss(req.getParameter("type"));
        String title = validateXss(req.getParameter("title"));
        String numOfHours = validateXss(req.getParameter("numOfHours"));
        String description = validateXss(req.getParameter("description"));
        String courseId = validateXss(req.getParameter("updateCourseId"));

        Course course = new Course();
        course.setType(type);
        course.setTitle(title);
        course.setNumOfHours(parseInt(numOfHours));
        course.setDescription(description);

        if (courseId != null) {
            course.setCourseId(parseInt(courseId));
        }

        return course;
    }
}
