package courses.builders;

import static courses.validation.Validator.validateXss;
import static java.lang.Integer.parseInt;

import courses.entities.Mark;
import courses.validation.ValidateException;

import javax.servlet.http.HttpServletRequest;

public class MarkBuilder {
    
    public static Mark build(HttpServletRequest req) throws ValidateException {
        String comment = validateXss(req.getParameter("comment"));
        String markParameter = validateXss(req.getParameter("mark"));
        String markId = validateXss(req.getParameter("updateMarkId"));

        Mark mark = new Mark();
        mark.setComment(comment);
        mark.setMark(parseInt(markParameter));

        if (markId != null) {
            mark.setMarkId(parseInt(markId));
        }

        return mark;
    }
}
