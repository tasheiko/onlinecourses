package courses.builders;

import static courses.validation.Validator.validateXss;
import static java.lang.Integer.parseInt;

import courses.entities.User;
import courses.entities.UserType;
import courses.validation.ValidateException;

import javax.servlet.http.HttpServletRequest;

public class UserBuilder {

    public static User buildUser(HttpServletRequest req) throws ValidateException {
        String email = validateXss(req.getParameter("email"));
        String password = validateXss(req.getParameter("password"));
        String name = validateXss(req.getParameter("name"));
        String surname = validateXss(req.getParameter("surname"));
        String address = validateXss(req.getParameter("address"));
        String phoneNum = validateXss(req.getParameter("phoneNum"));
        String type = validateXss(req.getParameter("userType"));
        String specialization = validateXss(req.getParameter("specialization"));
        String userId = validateXss(req.getParameter("updateUserId"));

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setName(name);
        user.setSurname(surname);
        user.setAddress(address);
        user.setPhoneNum(phoneNum);
        user.setType(UserType.valueOf(type));

        if (specialization != null) {
            user.setSpecialization(specialization);
        }

        if (userId != null) {
            user.setUserId(parseInt(userId));
        }

        return user;
    }
}
