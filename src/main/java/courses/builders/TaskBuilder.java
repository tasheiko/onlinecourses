package courses.builders;

import static courses.validation.Validator.validateXss;
import static java.lang.Integer.parseInt;

import courses.entities.Task;
import courses.validation.ValidateException;

import javax.servlet.http.HttpServletRequest;

public class TaskBuilder {

    public static Task build(HttpServletRequest req) throws ValidateException {
        String nameOfTask = validateXss(req.getParameter("nameOfTask"));
        String description = validateXss(req.getParameter("description"));
        String taskId = validateXss(req.getParameter("updateTaskId"));

        Task task = new Task();
        task.setNameOfTask(nameOfTask);
        task.setDescription(description);

        if (taskId != null) {
            task.setTaskId(parseInt(taskId));
        }

        return task;
    }
}