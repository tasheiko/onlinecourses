package courses.converters;

import courses.entities.Course;
import courses.entities.Task;
import courses.model.TaskDto;

public class TaskConverter {

    public static TaskDto toTaskDto(Task task, Course course) {
        TaskDto dto = new TaskDto();
        dto.setTaskId(task.getTaskId());
        dto.setNameOfTask(task.getNameOfTask());
        dto.setDescription(task.getDescription());
        dto.setType(course.getType());
        dto.setTitle(course.getTitle());

        return dto;
    }
}
