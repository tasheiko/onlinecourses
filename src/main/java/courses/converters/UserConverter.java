package courses.converters;

import courses.entities.User;
import courses.entities.UserType;
import courses.model.UserDto;

public class UserConverter {

    public static UserDto toUserDto(User user) {
        UserDto dto = new UserDto();
        dto.setUserId(user.getUserId());
        dto.setEmail(user.getEmail());
        dto.setName(user.getName());
        dto.setSurname(user.getSurname());
        dto.setAddress(user.getAddress());
        dto.setPhoneNum(user.getPhoneNum());
        if (!user.isAdmin()) {
            dto.setType(user.getType());
            if (user.getType().equals(UserType.TEACHER)) {
                dto.setSpecialization(user.getSpecialization());
            }
        }

        return dto;
    }
}