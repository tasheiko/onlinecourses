package courses.converters;

import courses.entities.Course;
import courses.entities.Teacher;
import courses.model.CourseDto;

public class CourseConverter {

    public static CourseDto toCourseDto(Course course, Teacher teacher) {
        CourseDto dto = new CourseDto();
        dto.setCourseId(course.getCourseId());
        dto.setDescription(course.getDescription());
        dto.setNumOfHours(course.getNumOfHours());
        dto.setTitle(course.getTitle());
        dto.setType(course.getType());
        dto.setName(teacher.getName());
        dto.setSurname(teacher.getSurname());
        dto.setUserId(teacher.getUserId());

        return dto;
    }

}
