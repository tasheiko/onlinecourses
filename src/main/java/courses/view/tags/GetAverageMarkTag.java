package courses.view.tags;

import courses.dao.DAOException;
import courses.services.MarkService;
import courses.services.MarkServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class GetAverageMarkTag extends TagSupport {

    private static final Logger logger = Logger.getLogger(GetAverageMarkTag.class.getName());
    private static MarkService markService = MarkServiceImpl.getInstance();
    private int courseId;

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    private static String averageMark(int courseId) throws DAOException {
        return markService.calculateAverageMark(courseId);
    }

    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.println(averageMark(courseId));
        } catch (IOException |
                DAOException e) {
            logger.error(e);
        }
        return SKIP_BODY;
    }
}
