package courses.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class TaskDto {

    private Integer taskId;
    private String nameOfTask;
    private String description;

    private String type;
    private String title;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean equals(final Object obj) {
        if (obj instanceof TaskDto) {
            final TaskDto other = (TaskDto) obj;
            return new EqualsBuilder()
                    .append(taskId, other.taskId)
                    .append(nameOfTask, other.nameOfTask)
                    .append(description, other.description)
                    .append(type, other.type)
                    .append(title, other.title)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(taskId)
                .append(nameOfTask)
                .append(description)
                .append(type)
                .append(title)
                .toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("taskId", taskId)
                .append("nameOfTask", nameOfTask)
                .append("description", description)
                .append("type", type)
                .append("title", title)
                .toString();
    }
}
