package courses.model;

import courses.entities.UserType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserDto {

    private Integer userId;
    private String email;
    private String name;
    private String surname;
    private String address;
    private String phoneNum;
    private UserType type;

    private String specialization;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("userId", userId)
                .append("email", email)
                .append("name", name)
                .append("surname", surname)
                .append("address", address)
                .append("phoneNum", phoneNum)
                .append("type", type)
                .append("specialization", specialization)
                .toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof UserDto) {
            final UserDto other = (UserDto) obj;
            return new EqualsBuilder()
                    .append(userId, other.userId)
                    .append(email, other.email)
                    .append(name, other.name)
                    .append(surname, other.surname)
                    .append(address, other.address)
                    .append(phoneNum, other.phoneNum)
                    .append(type, other.type)
                    .append(specialization, other.specialization)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(userId)
                .append(email)
                .append(name)
                .append(surname)
                .append(address)
                .append(phoneNum)
                .append(type)
                .append(specialization)
                .toHashCode();
    }
}