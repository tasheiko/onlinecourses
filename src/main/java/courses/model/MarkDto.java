package courses.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MarkDto {

    private Integer markId;
    private String comment;
    private Integer mark;

    private String studentName;
    private String studentSurname;

    private String nameOfTask;

    public Integer getMarkId() {
        return markId;
    }

    public void setMarkId(Integer markId) {
        this.markId = markId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSurname() {
        return studentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.studentSurname = studentSurname;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("markId", markId)
                .append("comment", comment)
                .append("mark", mark)
                .append("studentName", studentName)
                .append("studentSurname", studentSurname)
                .append("nameOfTask", nameOfTask)
                .toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof MarkDto) {
            final MarkDto other = (MarkDto) obj;
            return new EqualsBuilder()
                    .append(markId, other.markId)
                    .append(comment, other.comment)
                    .append(mark, other.mark)
                    .append(studentName, other.studentName)
                    .append(studentSurname, other.studentSurname)
                    .append(nameOfTask, other.nameOfTask)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(markId)
                .append(comment)
                .append(mark)
                .append(studentName)
                .append(studentSurname)
                .append(nameOfTask)
                .toHashCode();
    }
}
