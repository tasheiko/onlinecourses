package courses.web.invoker;

import courses.web.actions.Action;
import courses.web.actions.course.*;
import courses.web.actions.mark.*;
import courses.web.actions.student.StudentAction;
import courses.web.actions.task.*;
import courses.web.actions.teacher.TeacherAction;
import courses.web.actions.user.*;

import static courses.web.servlets.ServletHelper.*;

import java.util.HashMap;
import java.util.Map;

public class Invoker {

    private static Map<String, Action> actionMap = new HashMap<>();

    static {
        actionMap.put(getActionKey(SHOW_COURSES), new CourseAction());
        actionMap.put(getActionKey(CHANGE_COURSE_INFO), new AddCourseAction());
        actionMap.put(getActionKey(SHOW_UPDATED_COURSE), new UpdateCourseAction());
        actionMap.put(getActionKey(SHOW_DELETED_COURSE), new DeleteCourseAction());
        actionMap.put(getActionKey(SHOW_STUDENTS), new StudentAction());
        actionMap.put(getActionKey(SHOW_TEACHERS), new TeacherAction());
        actionMap.put(getActionKey(SHOW_TASKS), new TaskAction());
        actionMap.put(getActionKey(CHANGE_TASK_INFO), new AddTaskAction());
        actionMap.put(getActionKey(SHOW_UPDATED_TASK), new UpdateTaskAction());
        actionMap.put(getActionKey(SHOW_DELETED_TASK), new DeleteTaskAction());
        actionMap.put(getActionKey(SHOW_MARKS), new MarkAction());
        actionMap.put(getActionKey(CHANGE_MARK_INFO), new AddMarkAction());
        actionMap.put(getActionKey(SHOW_UPDATED_MARK), new UpdateMarkAction());
        actionMap.put(getActionKey(SHOW_DELETED_MARK), new DeleteMarkAction());
        actionMap.put(getActionKey(SHOW_USERS), new UserAction());
        actionMap.put(getActionKey(SHOW_REGISTRATION), new AddUserAction());
        actionMap.put(getActionKey(SHOW_UPDATED_USER), new UpdateUserAction());
        actionMap.put(getActionKey(SHOW_DELETED_USER), new DeleteUserAction());
        actionMap.put(getActionKey(SHOW_LOGIN), new LoginAction());
        actionMap.put(getActionKey(SHOW_LOGOUT), new LogoutAction());
    }

    public static Map<String, Action> getActionMap() {
        return actionMap;
    }
}
