package courses.web.servlets;

import static courses.dao.utils.ErrorCode.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCode.IO_EXCEPTION_CODE;

import static courses.web.invoker.Invoker.getActionMap;
import static courses.web.servlets.ServletHelper.SHOW_MENU;
import static courses.web.servlets.ServletHelper.CONTENT_TYPE;
import static courses.web.servlets.ServletHelper.getUrl;
import static courses.web.servlets.ServletHelper.getActionKey;
import static courses.web.servlets.ServletHelper.UTF;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.dao.DAOException;
import courses.validation.ValidateException;

import courses.web.actions.Action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;

public class CoursesServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(CoursesServlet.class.getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType(CONTENT_TYPE);
        req.setCharacterEncoding(UTF);

        try {
            if (getActionMap().get(getActionKey(getUrl(req))) != null) {
                executeCommand(req, resp, getActionKey(getUrl(req)));
            } else if (getUrl(req).equals(SHOW_MENU)) {
                showMenu(resp);
            } else {
                resourceError(req, resp);
            }
        } catch (ValidateException ve) {
            logger.error(ve);
            resp.setStatus(SC_BAD_REQUEST);
            validateInfo(req, resp, ve.getMessage());
        } catch (DAOException e) {
            logger.error(e);
            resp.setStatus(SC_INTERNAL_SERVER_ERROR);
            serverErrorInfo(req, resp, DAO_EXCEPTION_CODE.getCode(), e.getMessage());
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.setStatus(SC_INTERNAL_SERVER_ERROR);
            serverErrorInfo(req, resp, IO_EXCEPTION_CODE.getCode(), ioe.getMessage());
        }
    }

    private void executeCommand(HttpServletRequest req, HttpServletResponse resp, String actionKey)
            throws ServletException, IOException, DAOException, ValidateException {
        Action action = getActionMap().get(actionKey);
        action.execute(req, resp, getServletContext());
    }

    private void resourceError(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("resourceError", "Resource not found");
        getServletContext().getRequestDispatcher("/jsp/errors/resourceError.jsp").forward(req, resp);
    }

    private void validateInfo(HttpServletRequest req, HttpServletResponse resp, String errorMessage)
            throws ServletException, IOException {
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/validateError.jsp").forward(req, resp);
    }

    private void serverErrorInfo(HttpServletRequest req, HttpServletResponse resp, String errCode, String errorMessage)
            throws ServletException, IOException {

        req.setAttribute("errorCode", errCode);
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/serverError.jsp").forward(req, resp);
    }

    private void showMenu(HttpServletResponse resp) throws IOException {
        resp.sendRedirect("/onlineCourses/courses/menu");
    }
}
