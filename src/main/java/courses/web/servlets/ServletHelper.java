package courses.web.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public class ServletHelper extends HttpServlet {

    public static final String CONTENT_TYPE = "text/html; charset = utf-8";
    public static final String UTF = "UTF-8";

    public static final String ERROR = "/onlineCourses/courses/error";

    public static final String SHOW_USERS = "/courses/user";
    public static final String SHOW_REGISTRATION = "/courses/registration";
    public static final String SHOW_DELETED_USER = "/courses/deleteUser";
    public static final String SHOW_UPDATED_USER = "/courses/updateUser";
    public static final String SHOW_LOGIN = "/courses/login";
    public static final String SHOW_LOGOUT = "/courses/logout";

    public static final String SHOW_STUDENTS = "/courses/student";
    public static final String SHOW_TEACHERS = "/courses/teacher";

    static final String SHOW_MENU = "/courses";
    public static final String SHOW_COURSES = "/courses/course";
    public static final String SHOW_DELETED_COURSE = "/courses/deleteCourse";
    public static final String SHOW_UPDATED_COURSE = "/courses/updateCourse";
    public static final String CHANGE_COURSE_INFO = "/courses/changeCourse";

    public static final String SHOW_MARKS = "/courses/mark";
    public static final String SHOW_DELETED_MARK = "/courses/deleteMark";
    public static final String SHOW_UPDATED_MARK = "/courses/updateMark";
    public static final String CHANGE_MARK_INFO = "/courses/changeMark";

    public static final String SHOW_TASKS = "/courses/task";
    public static final String SHOW_DELETED_TASK = "/courses/deleteTask";
    public static final String SHOW_UPDATED_TASK = "/courses/updateTask";
    public static final String CHANGE_TASK_INFO = "/courses/changeTask";

    public static String getUrl(HttpServletRequest req) {
        return req.getRequestURI().substring(req.getContextPath().length());
    }

    public static String getActionKey(String actionKey) {
        StringBuilder str = new StringBuilder(actionKey);
        str.delete(0, 9);
        return str.toString();
    }
}