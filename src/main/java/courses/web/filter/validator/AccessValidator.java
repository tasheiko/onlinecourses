package courses.web.filter.validator;

import courses.entities.UserType;
import courses.web.filter.rules.Rule;

import java.util.Arrays;

public class AccessValidator {

    public static boolean isUrlAllowed(String url, UserType type) {
        if (type != null) {
            Rule rule = Rule.getAllowedUrl(url);
            if (rule != null) {
                return Arrays.asList(rule.getUserTypes()).contains(type);
            }
        }
        return false;
    }
}
