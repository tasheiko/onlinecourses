package courses.web.filter;

import static courses.web.servlets.ServletHelper.CONTENT_TYPE;
import static courses.web.servlets.ServletHelper.getUrl;
import static courses.web.servlets.ServletHelper.ERROR;
import static courses.web.servlets.ServletHelper.UTF;

import courses.entities.User;
import courses.entities.UserType;
import courses.web.filter.validator.AccessValidator;

import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SecurityFilter implements Filter {

    public void init(FilterConfig config) {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding(UTF);

        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute("user");

        String url = getUrl(request);
        UserType userType = getUserType(currentUser);

        if (currentUser != null && currentUser.isAdmin() || AccessValidator.isUrlAllowed(url, userType)) {
            chain.doFilter(req, resp);
        } else {
            response.sendRedirect(ERROR);
        }
    }

    private UserType getUserType(User currentUser) {
        if (currentUser == null) {
            return UserType.UNAUTHORIZED;
        }
        return currentUser.getType();
    }

    public void destroy() {
    }
}