package courses.web.filter.rules;

import static courses.entities.UserType.STUDENT;
import static courses.entities.UserType.TEACHER;
import static courses.entities.UserType.UNAUTHORIZED;

import courses.entities.UserType;

public enum Rule {
    SHOW_DELETED_MARK("/courses/deleteMark", new UserType[]{TEACHER}),
    SHOW_UPDATED_MARK("/courses/updateMark", new UserType[]{TEACHER}),
    CHANGE_MARK_INFO("/courses/changeMark", new UserType[]{TEACHER}),
    SHOW_STUDENTS("/courses/student", new UserType[]{TEACHER}),

    SHOW_LOGIN("/courses/login", new UserType[]{STUDENT, TEACHER, UNAUTHORIZED}),
    SHOW_TEACHERS("/courses/teacher", new UserType[]{STUDENT, TEACHER}),
    SHOW_COURSES("/courses/course", new UserType[]{STUDENT, TEACHER}),
    SHOW_MARKS("/courses/mark", new UserType[]{STUDENT, TEACHER}),
    SHOW_TASKS("/courses/task", new UserType[]{STUDENT, TEACHER}),
    ACCESS_WARNING("/onlineCourses/accessWarning", new UserType[]{STUDENT, TEACHER}),
    LOGOUT("/courses/logout", new UserType[]{STUDENT, TEACHER}),
    MENU("/courses/menu", new UserType[]{STUDENT, TEACHER, UNAUTHORIZED}),
    ERROR("/courses/error", new UserType[]{STUDENT, TEACHER}),
    REGISTRATION("/registration", new UserType[]{STUDENT, TEACHER, UNAUTHORIZED}),
    REGISTRATION_RESULT("/courses/registration", new UserType[]{STUDENT, TEACHER, UNAUTHORIZED}),
    CREATE_MARK("/courses/createMarks", new UserType[]{TEACHER});

    private String url;
    private UserType[] userTypes;

    public String getUrl() {
        return url;
    }

    public UserType[] getUserTypes() {
        return userTypes;
    }

    Rule(final String url, final UserType[] userTypes) {
        this.url = url;
        this.userTypes = userTypes;
    }

    /**
     * Return userType array if url is allowed to teacher/student,
     * otherwise return null
     */
    public static Rule getAllowedUrl(final String url) {
        for (final Rule rule : values()) {
            if (rule.getUrl().equalsIgnoreCase(url)) {
                return rule;
            }
        }
        return null;
    }
}
