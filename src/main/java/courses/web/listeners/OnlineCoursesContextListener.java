package courses.web.listeners;

import courses.dao.utils.ConnectionPool;
import courses.web.servlets.CoursesServlet;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.SQLException;

public class OnlineCoursesContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(CoursesServlet.class.getName());

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        try {
            ConnectionPool.closeConnectionPool();
        } catch (SQLException e) {
            logger.error(e);
        }
        System.out.println("ServletContextListener destroyed");
    }


    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
            ConnectionPool.createConnectionPool();
        } catch (ClassNotFoundException | SQLException e) {
            logger.error(e);
        }
        System.out.println("ServletContextListener started");
    }
}
