package courses.web.actions.student;

import courses.dao.DAOException;
import courses.services.UserServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StudentAction implements Action {

    private static UserServiceImpl userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("students", userService.findAllStudents());
        servletContext.getRequestDispatcher("/jsp/students/showStudentsList.jsp").forward(req, resp);
    }
}
