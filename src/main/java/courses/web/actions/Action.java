package courses.web.actions;

import courses.dao.DAOException;
import courses.validation.ValidateException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Action {
    void execute(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException;
}
