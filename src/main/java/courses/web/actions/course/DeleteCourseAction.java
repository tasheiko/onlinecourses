package courses.web.actions.course;

import courses.dao.DAOException;
import courses.entities.Course;
import courses.services.CourseService;
import courses.services.CourseServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class DeleteCourseAction implements Action {

    private static CourseService courseService = CourseServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        String courseId = req.getParameter("deleteCourseId");
        Course deletedCourse = courseService.deleteCourse(parseInt(courseId));

        req.setAttribute("courseDel", deletedCourse);
        servletContext.getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }
}
