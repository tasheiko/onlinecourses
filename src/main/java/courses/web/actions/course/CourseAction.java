package courses.web.actions.course;

import courses.dao.DAOException;
import courses.services.CourseService;
import courses.services.CourseServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CourseAction implements Action {

    private static CourseService courseService = CourseServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("entitiesList", courseService.getCoursesDto());
        servletContext.getRequestDispatcher("/jsp/courses/showCoursesList.jsp").forward(req, resp);
    }
}
