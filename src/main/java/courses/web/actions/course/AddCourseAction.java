package courses.web.actions.course;

import courses.builders.CourseBuilder;
import courses.dao.DAOException;
import courses.entities.Course;
import courses.services.CourseService;
import courses.services.CourseServiceImpl;
import courses.validation.ValidateException;
import courses.validation.Validator;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class AddCourseAction implements Action {

    private static CourseService courseService = CourseServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException {

        Course course = CourseBuilder.build(req);
        Validator.validateCourse(course);
        int teacherId = parseInt(req.getParameter("teacherId"));
        boolean result = courseService.create(course, teacherId);

        req.setAttribute("changeCourse", result);
        servletContext.getRequestDispatcher("/jsp/courses/createCourseResult.jsp").forward(req, resp);
    }
}
