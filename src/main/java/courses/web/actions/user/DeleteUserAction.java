package courses.web.actions.user;

import static java.lang.Integer.parseInt;

import courses.dao.DAOException;
import courses.entities.User;
import courses.services.UserService;
import courses.services.UserServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteUserAction implements Action {

    private static UserService userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        String userId = req.getParameter("deleteUserId");
        User deletedUser = userService.deleteUser(parseInt(userId));

        req.setAttribute("userDel", deletedUser);
        servletContext.getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }
}
