package courses.web.actions.user;

import courses.builders.UserBuilder;
import courses.dao.DAOException;
import courses.entities.User;
import courses.services.UserService;
import courses.services.UserServiceImpl;
import courses.validation.ValidateException;
import courses.validation.Validator;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateUserAction implements Action {

    private static UserService userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException {

        User user = UserBuilder.buildUser(req);
        user = userService.setPassword(user);
        Validator.validateUser(user);
        userService.update(user);

        req.setAttribute("updatedUser", user);
        servletContext.getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
    }
}
