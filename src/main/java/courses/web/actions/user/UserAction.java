package courses.web.actions.user;

import courses.dao.DAOException;
import courses.services.UserService;
import courses.services.UserServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAction implements Action {

    private static UserService userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("users", userService.getUsersDto());
        servletContext.getRequestDispatcher("/jsp/users/showUsersList.jsp").forward(req, resp);
    }
}
