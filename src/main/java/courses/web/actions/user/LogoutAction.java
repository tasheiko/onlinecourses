package courses.web.actions.user;

import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutAction implements Action {

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws ServletException, IOException {

        req.getSession().removeAttribute("user");
        req.getSession().invalidate();
        servletContext.getRequestDispatcher("/jsp/general/logout.jsp").forward(req, resp);
    }
}
