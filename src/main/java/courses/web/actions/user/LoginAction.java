package courses.web.actions.user;

import courses.dao.DAOException;
import courses.entities.User;
import courses.services.UserService;
import courses.services.UserServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginAction implements Action {

    private static UserService userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        String email = req.getParameter("userEmail");
        String password = req.getParameter("userPassword");
        boolean result = userService.authenticateUser(email, password);

        if (result) {
            User user = userService.getUserByEmail(email);
            req.getSession().setAttribute("user", user);
            servletContext.getRequestDispatcher("/courses/menu").forward(req, resp);
        } else {
            req.setAttribute("loginError", "error");
            servletContext.getRequestDispatcher("/courses/error").forward(req, resp);
        }
    }
}
