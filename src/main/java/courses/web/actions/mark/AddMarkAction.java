package courses.web.actions.mark;

import courses.builders.MarkBuilder;
import courses.dao.DAOException;
import courses.entities.Mark;
import courses.services.MarkService;
import courses.services.MarkServiceImpl;
import courses.validation.ValidateException;
import courses.validation.Validator;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class AddMarkAction implements Action {

    private static MarkService markService = MarkServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException {

        Mark mark = MarkBuilder.build(req);
        Validator.validateMark(mark);
        int studentId = parseInt(req.getParameter("studentId"));
        int taskId = parseInt(req.getParameter("taskId"));
        boolean result = markService.create(mark, studentId, taskId);

        req.setAttribute("changeMark", result);
        servletContext.getRequestDispatcher("/jsp/marks/createMarkResult.jsp").forward(req, resp);
    }
}
