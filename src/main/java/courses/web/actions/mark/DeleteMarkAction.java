package courses.web.actions.mark;

import courses.dao.DAOException;
import courses.entities.Mark;
import courses.services.MarkService;
import courses.services.MarkServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class DeleteMarkAction implements Action {

    private static MarkService markService = MarkServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        String markId = req.getParameter("deleteMarkId");
        Mark deletedMark = markService.deleteMark(parseInt(markId));

        req.setAttribute("markDel", deletedMark);
        servletContext.getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }
}