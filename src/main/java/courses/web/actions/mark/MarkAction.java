package courses.web.actions.mark;

import courses.dao.DAOException;
import courses.services.MarkService;
import courses.services.MarkServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MarkAction implements Action {

    private static MarkService markService = MarkServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("entitiesList", markService.getMarksDto());
        servletContext.getRequestDispatcher("/jsp/marks/showMarksList.jsp").forward(req, resp);
    }
}