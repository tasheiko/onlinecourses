package courses.web.actions.teacher;

import courses.dao.DAOException;
import courses.services.UserServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TeacherAction implements Action {

    private static UserServiceImpl userService = UserServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("teachers", userService.findAllTeachers());
        servletContext.getRequestDispatcher("/jsp/teachers/showTeachersList.jsp").forward(req, resp);
    }
}
