package courses.web.actions.task;

import courses.builders.TaskBuilder;
import courses.dao.DAOException;
import courses.entities.Task;
import courses.services.TaskService;
import courses.services.TaskServiceImpl;
import courses.validation.ValidateException;
import courses.validation.Validator;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateTaskAction implements Action {

    private static TaskService taskService = TaskServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException {

        Task task = TaskBuilder.build(req);
        Validator.validateTask(task);
        taskService.update(task);

        req.setAttribute("updatedTask", task);
        servletContext.getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
    }
}
