package courses.web.actions.task;

import static java.lang.Integer.parseInt;

import courses.builders.TaskBuilder;
import courses.dao.DAOException;
import courses.entities.Task;
import courses.services.TaskService;
import courses.services.TaskServiceImpl;
import courses.validation.ValidateException;
import courses.validation.Validator;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddTaskAction implements Action {

    private static TaskService taskService = TaskServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException, ValidateException {

        Task task = TaskBuilder.build(req);
        Validator.validateTask(task);
        int courseId = parseInt(req.getParameter("courseId"));
        boolean result = taskService.create(task, courseId);

        req.setAttribute("changeTask", result);
        servletContext.getRequestDispatcher("/jsp/tasks/createTaskResult.jsp").forward(req, resp);
    }
}
