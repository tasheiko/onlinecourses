package courses.web.actions.task;

import courses.dao.DAOException;
import courses.services.TaskService;
import courses.services.TaskServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TaskAction implements Action {

    private static TaskService taskService = TaskServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        req.setAttribute("tasks", taskService.getTasksDto());
        servletContext.getRequestDispatcher("/jsp/tasks/showTasksList.jsp").forward(req, resp);
    }
}
