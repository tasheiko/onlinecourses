package courses.web.actions.task;

import static java.lang.Integer.parseInt;

import courses.dao.DAOException;
import courses.entities.Task;
import courses.services.TaskService;
import courses.services.TaskServiceImpl;
import courses.web.actions.Action;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteTaskAction implements Action {

    private static TaskService taskService = TaskServiceImpl.getInstance();

    public void execute(HttpServletRequest req, HttpServletResponse resp, ServletContext servletContext)
            throws DAOException, ServletException, IOException {

        String taskId = req.getParameter("deleteTaskId");
        Task deletedTask = taskService.deleteTask(parseInt(taskId));

        req.setAttribute("taskDel", deletedTask);
        servletContext.getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }
}