package courses.services;

import static courses.converters.UserConverter.toUserDto;

import courses.dao.DAOException;
import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.User;
import courses.dao.impl.UserDaoImpl;
import courses.model.UserDto;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDaoImpl dao;

    private static UserServiceImpl INSTANCE;

    private UserServiceImpl() {
        dao = new UserDaoImpl();
    }

    public static UserServiceImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserServiceImpl();
        }
        return INSTANCE;
    }

    public void update(User entity) throws DAOException {
        dao.update(entity);
    }

    public void delete(int userId) throws DAOException {
        dao.delete(userId);
    }

    public List<User> findAll() throws DAOException {
        return dao.findAll();
    }

    public User findById(int userId) throws DAOException {
        return dao.findById(userId);
    }

    public Boolean isExists(User course) throws DAOException {
        return dao.isExists(course);
    }

    public void add(User user) throws DAOException {
        dao.add(user);
    }

    public boolean authenticateUser(String email, String password) throws DAOException {
        User user = getUserByEmail(email);
        return (user != null) && user.getEmail().equals(email) && user.getPassword().equals(password);
    }

    public User getUserByEmail(String email) throws DAOException {
        return dao.getUserByEmail(email);
    }

    public Student findStudentById(Integer id) throws DAOException {
        return dao.findStudentById(id);
    }

    public List<Student> findAllStudents() throws DAOException {
        return dao.findAllStudents();
    }

    public Teacher findTeacherById(Integer id) throws DAOException {
        return dao.findTeacherById(id);
    }

    public List<Teacher> findAllTeachers() throws DAOException {
        return dao.findAllTeachers();
    }

    public boolean create(User user) throws DAOException {
        boolean result = !isExists(user);

        if (result) {
            add(user);
        }
        return result;
    }

    public User deleteUser(int userId) throws DAOException {
        User deletedUser = findById(userId);
        delete(userId);

        return deletedUser;
    }

    public List<UserDto> getUsersDto() throws DAOException {
        List<UserDto> list = new ArrayList<>();

        for (User user : findAll()) {
            UserDto userDto = toUserDto(user);
            list.add(userDto);
        }

        return list;
    }

    public User setPassword(User user) throws DAOException {
        user.setPassword(findById(user.getUserId()).getPassword());
        return user;
    }
}
