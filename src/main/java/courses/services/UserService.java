package courses.services;

import courses.dao.DAOException;
import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.User;
import courses.model.UserDto;

import java.util.List;

public interface UserService {

    /**
     * Update user
     *
     * @param entity - selected user
     */
    void update(User entity) throws DAOException;

    /**
     * Delete user by id
     *
     * @param userId - selected identifier
     */
    void delete(int userId) throws DAOException;

    /**
     * Find all users from DB
     *
     * @return list of all users
     */
    List<User> findAll() throws DAOException;

    /**
     * Find user by id
     *
     * @param userId - selected identifier
     * @return found user
     */
    User findById(int userId) throws DAOException;

    /**
     * Check is user exists in DB
     *
     * @param course - selected course
     * @return result is user exists in DB
     */
    Boolean isExists(User course) throws DAOException;

    /**
     * Add user into DB
     *
     * @param user - selected user
     */
    void add(User user) throws DAOException;

    /**
     * Check is user valid
     *
     * @param email - selected email
     * @param password - selected password
     * @return result is user valid
     */
    boolean authenticateUser(String email, String password) throws DAOException;

    /**
     * Get user by email
     *
     * @param email - selected email
     * @return found user
     */
    User getUserByEmail(String email) throws DAOException;

    /**
     * Add user into DB
     *
     * @param user - selected user
     * @return result is added user exists in DB
     */
    boolean create(User user) throws DAOException;

    /**
     * Delete user by id
     *
     * @param userId - selected identifier
     * @return deleted user
     */
    User deleteUser(int userId) throws DAOException;

    /**
     * Convert users to users dto
     *
     * @return list of all users converted in dto
     */
    List<UserDto> getUsersDto() throws DAOException;

    /**
     * Set password into user
     *
     * @param user - selected user
     * @return user with set password
     */
    User setPassword(User user) throws DAOException;

    /**
     * Find student by id
     *
     * @param id - selected identifier
     * @return found student
     */
    Student findStudentById(Integer id) throws DAOException;

    /**
     * Find all students from DB
     *
     * @return list of all students
     */
    List<Student> findAllStudents() throws DAOException;

    /**
     * Find teacher by id
     *
     * @param id - selected identifier
     * @return found teacher
     */
    Teacher findTeacherById(Integer id) throws DAOException;

    /**
     * Find all teachers from DB
     *
     * @return list of all teachers
     */
    List<Teacher> findAllTeachers() throws DAOException;
}
