package courses.services;

import courses.dao.DAOException;
import courses.entities.Mark;
import courses.model.MarkDto;

import java.util.List;

public interface MarkService {

    /**
     * Update selected mark
     *
     * @param entity - selected mark
     */
    void update(Mark entity) throws DAOException;

    /**
     * delete mark by id
     *
     * @param markId - selected identifier
     */
    void delete(int markId) throws DAOException;

    /**
     * Find all marks from DB
     *
     * @return list of all marks
     */
    List<Mark> findAll() throws DAOException;

    /**
     * Find mark by id
     *
     * @param markId - selected identifier
     * @return found mark
     */
    Mark findById(int markId) throws DAOException;

    /**
     * Add new mark in DB
     *
     * @param entity - selected mark
     */
    void add(Mark entity) throws DAOException;

    /**
     * Delete mark by id
     *
     * @param markId - selected identifier
     * @return deleted mark
     */
    Mark deleteMark(int markId) throws DAOException;

    /**
     * Add relationships into mark and add it to DB
     *
     * @param mark - selected mark
     * @param studentId - selected identifier of student
     * @param taskId - selected identifier of task
     * @return is created mark exists in DB
     */
    boolean create(Mark mark, int studentId, int taskId) throws DAOException;

    /**
     * Convert all marks into marks dto
     *
     * @return all marks dto
     */
    List<MarkDto> getMarksDto() throws DAOException;

    /**
     * Calculate average mark
     *
     * @param courseId - selected course identifier
     * @return String representation of average mark
     */
    String calculateAverageMark(int courseId) throws DAOException;

}
