package courses.services;

import static courses.converters.CourseConverter.toCourseDto;

import courses.dao.DAOException;
import courses.dao.impl.CourseDaoImpl;
import courses.entities.Course;
import courses.entities.Teacher;
import courses.model.CourseDto;

import java.util.ArrayList;
import java.util.List;

public class CourseServiceImpl implements CourseService {

    private CourseDaoImpl dao;

    private static CourseServiceImpl INSTANCE;
    private static UserServiceImpl userService;

    private CourseServiceImpl() {
        dao = new CourseDaoImpl();
        userService = UserServiceImpl.getInstance();
    }

    public static CourseServiceImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CourseServiceImpl();
        }
        return INSTANCE;
    }

    public void update(Course entity) throws DAOException {
        dao.update(entity);
    }

    public void delete(int courseId) throws DAOException {
        dao.delete(courseId);
    }

    public List<Course> findAll() throws DAOException {
        return dao.findAll();
    }

    public Course findById(int courseId) throws DAOException {
        return dao.findById(courseId);
    }

    private Boolean isExists(Course course) throws DAOException {
        return dao.isExists(course);
    }

    public void add(Course course) throws DAOException {
        dao.add(course);
    }

    public Course deleteCourse(int courseId) throws DAOException {
        Course deletedCourse = findById(courseId);
        delete(courseId);

        return deletedCourse;
    }

    public Course addRelationship(Course course, Integer teacherId) throws DAOException {
        Teacher teacher = userService.findTeacherById(teacherId);
        course.setTeacher(teacher);
        return course;
    }

    public boolean create(Course course, int teacherId) throws DAOException {
        Course entity = addRelationship(course, teacherId);
        boolean result = !isExists(entity);

        if (result) {
            add(entity);
        }
        return result;
    }


    public List<CourseDto> getCoursesDto() throws DAOException {
        List<CourseDto> list = new ArrayList<>();

        for (Course course : findAll()) {
            Teacher teacher = getRelatedTeacher(course);
            CourseDto courseDto = toCourseDto(course, teacher);
            list.add(courseDto);
        }

        return list;
    }

    private Teacher getRelatedTeacher(Course course) throws DAOException {
        return userService.findTeacherById(course.getTeacher().getUserId());
    }
}
