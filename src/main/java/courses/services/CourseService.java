package courses.services;

import courses.dao.DAOException;
import courses.entities.Course;
import courses.model.CourseDto;

import java.util.List;

public interface CourseService {

    /**
     * Update selected course
     *
     * @param entity - selected entity
     */
    void update(Course entity) throws DAOException;

    /**
     * Delete course by selected id
     *
     * @param courseId - selected identifier
     */
    void delete(int courseId) throws DAOException;

    /**
     * Find all courses
     *
     * @return list of all courses
     */
    List<Course> findAll() throws DAOException;

    /**
     * Find course by id
     *
     * @param courseId - selected identifier
     * @return found course
     */
    Course findById(int courseId) throws DAOException;

    /**
     * Add new course in DB
     *
     * @param course - selected course
     */
    void add(Course course) throws DAOException;

    /**
     * Delete course by id
     *
     * @param courseId - selected identifier
     * @return deleted course
     */
    Course deleteCourse(int courseId) throws DAOException;

    /**
     * Add relationship into course
     *
     * @param course - selected course
     * @param teacherId - selected identifier of teacher
     * @return course with added relationship
     */
    Course addRelationship(Course course, Integer teacherId) throws DAOException;

    /**
     * Add relationship to course entity and add(create) it in DB
     *
     * @param course - selected course
     * @param teacherId - selected identifier of teacher
     * @return result is course exists in DB
     */
    boolean create(Course course, int teacherId) throws DAOException;

    /**
     * Convert all courses into courses dto
     *
     * @return all courses dto
     */
    List<CourseDto> getCoursesDto() throws DAOException;

}
