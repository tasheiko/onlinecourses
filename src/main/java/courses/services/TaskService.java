package courses.services;

import courses.dao.DAOException;
import courses.entities.Task;
import courses.model.TaskDto;

import java.util.List;

public interface TaskService {

    /**
     * Update selected task
     *
     * @param entity - selected task
     */
    void update(Task entity) throws DAOException;

    /**
     * delete task by id
     *
     * @param taskId - selected identifier
     */
    void delete(int taskId) throws DAOException;

    /**
     * Find all tasks
     *
     * @return list of all tasks
     */
    List<Task> findAll() throws DAOException;

    /**
     * Find task by id
     *
     * @param taskId - selected identifier
     * @return found task
     */
    Task findById(int taskId) throws DAOException;

    /**
     * Add selected task into DB
     *
     * @param entity - selected task
     */
    void add(Task entity) throws DAOException;

    /**
     * Add relationship to task and add it into DB
     *
     * @param task - selected task
     * @param courseId - selected identifier of course
     * @return added task
     */
    boolean create(Task task, int courseId) throws DAOException;

    /**
     * Delete task by id
     *
     * @param taskId - selected identifier
     * @return deleted task
     */
    Task deleteTask(int taskId) throws DAOException;

    /**
     * Add relationship to task
     *
     * @param task - selected task
     * @param courseId - selected identifier
     * @return task with added relationship
     */
    Task addRelationship(Task task, int courseId) throws DAOException;

    /**
     * Convert all tasks into tasks dto
     *
     * @return all tasks dto
     */
    List<TaskDto> getTasksDto() throws DAOException;
}
