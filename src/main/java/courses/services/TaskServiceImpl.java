package courses.services;

import static courses.converters.TaskConverter.toTaskDto;

import courses.dao.DAOException;
import courses.dao.impl.TaskDaoImpl;
import courses.entities.Course;
import courses.entities.Task;
import courses.model.TaskDto;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceImpl implements TaskService {

    private TaskDaoImpl dao;

    private static TaskServiceImpl INSTANCE;
    private static CourseServiceImpl courseService;

    private TaskServiceImpl() {
        dao = new TaskDaoImpl();
        courseService = CourseServiceImpl.getInstance();
    }

    public static TaskServiceImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TaskServiceImpl();
        }
        return INSTANCE;
    }

    public void update(Task entity) throws DAOException {
        Task task = addRelationship(entity);
        dao.update(task);
    }

    public void delete(int taskId) throws DAOException {
        dao.delete(taskId);
    }

    public List<Task> findAll() throws DAOException {
        return dao.findAll();
    }

    public Task findById(int taskId) throws DAOException {
        return dao.findById(taskId);
    }

    private Boolean isExists(Task entity) throws DAOException {
        return dao.isExists(entity);
    }

    public void add(Task entity) throws DAOException {
        dao.add(entity);
    }

    public boolean create(Task task, int courseId) throws DAOException {
        Task entity = addRelationship(task, courseId);
        boolean result = !isExists(entity);

        if (result) {
            add(entity);
        }
        return result;
    }

    public Task deleteTask(int taskId) throws DAOException {
        Task deletedTask = findById(taskId);
        delete(taskId);

        return deletedTask;
    }

    public Task addRelationship(Task task, int courseId) throws DAOException {
        Course course = courseService.findById(courseId);
        task.setCourse(course);
        return task;
    }

    private Task addRelationship(Task task) throws DAOException {
        Task tk = dao.findById(task.getTaskId());
        task.setCourse(tk.getCourse());
        return task;
    }

    public List<TaskDto> getTasksDto() throws DAOException {
        List<TaskDto> list = new ArrayList<>();

        for (Task task : findAll()) {
            Course course = getRelatedCourse(task);
            TaskDto taskDto = toTaskDto(task, course);
            list.add(taskDto);
        }

        return list;
    }

    private Course getRelatedCourse(Task task) throws DAOException {
        return courseService.findById(task.getCourse().getCourseId());
    }
}