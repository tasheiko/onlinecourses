package courses.services;

import static courses.converters.MarkConverter.toMarkDto;
import static java.lang.String.valueOf;

import courses.dao.DAOException;
import courses.dao.impl.MarkDaoImpl;
import courses.entities.Mark;
import courses.entities.Student;
import courses.entities.Task;
import courses.model.MarkDto;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MarkServiceImpl implements MarkService {

    private MarkDaoImpl dao;
    private static MarkServiceImpl INSTANCE;
    private static UserServiceImpl userService;

    private MarkServiceImpl() {
        dao = new MarkDaoImpl();
        userService = UserServiceImpl.getInstance();
    }

    public static MarkServiceImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MarkServiceImpl();
        }
        return INSTANCE;
    }

    public void update(Mark entity) throws DAOException {
        Mark mark = addRelationships(entity);
        dao.update(mark);
    }

    public void delete(int markId) throws DAOException {
        dao.delete(markId);
    }

    public List<Mark> findAll() throws DAOException {
        return dao.findAll();
    }

    public Mark findById(int markId) throws DAOException {
        return dao.findById(markId);
    }

    private Boolean isExists(Mark entity) throws DAOException {
        return dao.isExists(entity);
    }

    public void add(Mark entity) throws DAOException {
        dao.add(entity);
    }

    public Mark deleteMark(int markId) throws DAOException {
        Mark deletedMark = findById(markId);
        delete(markId);

        return deletedMark;
    }

    public boolean create(Mark mark, int studentId, int taskId) throws DAOException {
        Mark entity = addRelationships(mark, studentId, taskId);
        boolean result = !isExists(entity);

        if (result) {
            add(entity);
        }
        return result;
    }

    private Mark addRelationships(Mark mark, Integer studentId, Integer taskId) throws DAOException {
        Student student = userService.findStudentById(studentId);
        Task task = TaskServiceImpl.getInstance().findById(taskId);
        mark.setStudent(student);
        mark.setTask(task);
        return mark;
    }

    private Mark addRelationships(Mark mark) throws DAOException {
        Mark fb = dao.findById(mark.getMarkId());
        mark.setStudent(fb.getStudent());
        mark.setTask(fb.getTask());
        return mark;
    }

    public List<MarkDto> getMarksDto() throws DAOException {
        List<MarkDto> list = new ArrayList<>();

        for (Mark mark : findAll()) {
            Student student = getRelatedStudent(mark);
            Task task = getRelatedTask(mark);
            MarkDto markDto = toMarkDto(mark, student, task);
            list.add(markDto);
        }

        return list;
    }

    private Student getRelatedStudent(Mark mark) throws DAOException {
        return userService.findStudentById(mark.getStudent().getUserId());
    }

    private Task getRelatedTask(Mark mark) throws DAOException {
        return TaskServiceImpl.getInstance().findById(mark.getTask().getTaskId());
    }

    public String calculateAverageMark(int courseId) throws DAOException {
        List<Mark> marks = dao.findMarksByCourseId(courseId);

        String result;
        double count = 0.0;
        int sum = 0;
        DecimalFormat df = new DecimalFormat("##.#");
        if (marks.size() > 0) {
            for (Mark mark : marks) {
                sum += mark.getMark();
                count++;
            }
            result = valueOf(df.format(sum / count));
        } else {
            result = "this course has no marks yet";
        }

        return result;
    }

}
