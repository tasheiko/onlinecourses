package courses.entities;

import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Teacher extends User {

    /**
     * one-to-many relationship with Course
     */
    private Set<Course> courses;

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("specialization", getSpecialization()).
                        toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof Teacher) {
            final Teacher other = (Teacher) obj;
            return new EqualsBuilder()
                    .appendSuper(super.equals(obj))
                    .append(getSpecialization(), other.getSpecialization())
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(getSpecialization())
                .toHashCode();
    }
}