package courses.entities;

import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Task {

    /**
     * task id
     */
    private Integer taskId;

    /**
     * task name
     */
    private String nameOfTask;

    /**
     * task description
     */
    private String description;

    /**
     * many-to-one relationship with Course
     */
    private Course course;

    /**
     * one-to-many relationship with Mark
     */
    private Set<Mark> marks;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Set<Mark> getMarks() {
        return marks;
    }

    public void setMarks(Set<Mark> marks) {
        this.marks = marks;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("name of task", nameOfTask).
                append("description", description).
                toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof Task) {
            final Task other = (Task) obj;
            return new EqualsBuilder()
                    .append(taskId, other.taskId)
                    .append(nameOfTask, other.nameOfTask)
                    .append(description, other.description)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(taskId)
                .append(nameOfTask)
                .append(description)
                .toHashCode();
    }
}