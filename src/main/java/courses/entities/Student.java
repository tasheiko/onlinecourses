package courses.entities;

import java.util.Set;

public class Student extends User {

    /**
     * one-to-many relationship with Mark
     */
    private Set<Mark> marks;

    public Set<Mark> getMarks() {
        return marks;
    }

    public void setMarks(Set<Mark> marks) {
        this.marks = marks;
    }
}