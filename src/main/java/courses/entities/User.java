package courses.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class User {

    /**
     * user id
     */
    private Integer userId;

    /**
     * user email
     */
    private String email;

    /**
     * user password
     */
    private String password;

    /**
     * user name
     */
    private String name;

    /**
     * user surname
     */
    private String surname;

    /**
     * user address
     */
    private String address;

    /**
     * user phone number
     */
    private String phoneNum;

    /**
     * type of user: student or teacher
     */
    private UserType type;

    /**
     * check: whether the user is an administrator
     */
    private boolean admin;

    /**
     * teacher specialization
     */
    private String specialization;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public boolean isAdmin() {
        return this.admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("name", name).
                append("surname", surname).
                append("address", address).
                append("email", email).
                append("phone number", phoneNum).
                append("user type", type).
                toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof User) {
            final User other = (User) obj;
            return new EqualsBuilder()
                    .append(userId, other.userId)
                    .append(email, other.email)
                    .append(password, other.password)
                    .append(name, other.name)
                    .append(surname, other.surname)
                    .append(address, other.address)
                    .append(phoneNum, other.phoneNum)
                    .append(type, other.type)
                    .append(admin, other.admin)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(userId)
                .append(email)
                .append(password)
                .append(name)
                .append(surname)
                .append(address)
                .append(phoneNum)
                .append(type)
                .append(admin)
                .toHashCode();
    }
}