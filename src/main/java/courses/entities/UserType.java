package courses.entities;

public enum UserType {
    TEACHER("TEACHER"),
    STUDENT("STUDENT"),
    UNAUTHORIZED("UNAUTHORIZED");

    private String type;

    UserType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}