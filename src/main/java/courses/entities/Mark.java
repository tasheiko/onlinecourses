package courses.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Mark {

    /**
     * mark id
     */
    private Integer markId;

    /**
     * comment
     */
    private String comment;

    /**
     * mark
     */
    private Integer mark;

    /**
     * many-to-one relationship with Student
     */
    private Student student;

    /**
     * many-to-one relationship with Task
     */
    private Task task;

    public Integer getMarkId() {
        return markId;
    }

    public void setMarkId(Integer markId) {
        this.markId = markId;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("mark", mark).
                append("comment", comment).
                toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof Mark) {
            final Mark other = (Mark) obj;
            return new EqualsBuilder()
                    .append(markId, other.markId)
                    .append(comment, other.comment)
                    .append(mark, other.mark)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(markId)
                .append(comment)
                .append(mark)
                .toHashCode();
    }
}