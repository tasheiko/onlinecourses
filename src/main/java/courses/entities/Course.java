package courses.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

public class Course {

    /**
     * course id
     */
    private Integer courseId;

    /**
     * type of course
     */
    private String type;

    /**
     * name of course
     */
    private String title;

    /**
     * number of hours
     */
    private Integer numOfHours;

    /**
     * course description
     */
    private String description;

    /**
     * one-to-many relationship with Task
     */
    private Set<Task> tasks;

    /**
     * many-to-one relationship with Teacher
     */
    private Teacher teacher;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumOfHours() {
        return numOfHours;
    }

    public void setNumOfHours(Integer numOfHours) {
        this.numOfHours = numOfHours;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("type", type).
                append("title", title).
                append("numOfHours", numOfHours).
                append("description", description).
                toString();
    }

    public boolean equals(final Object obj) {
        if (obj instanceof Course) {
            final Course other = (Course) obj;
            return new EqualsBuilder()
                    .append(courseId, other.courseId)
                    .append(type, other.type)
                    .append(title, other.title)
                    .append(numOfHours, other.numOfHours)
                    .append(description, other.description)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(courseId)
                .append(type)
                .append(title)
                .append(numOfHours)
                .append(description)
                .toHashCode();
    }
}
