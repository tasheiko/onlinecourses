package courses.dao.impl;

import static courses.dao.utils.DaoUtil.getPreparedStatement;

import static java.lang.String.valueOf;

import courses.dao.DAOException;
import courses.dao.UserDao;
import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.User;
import courses.entities.UserType;
import org.apache.commons.lang3.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final String USER_BY_EMAIL = "SELECT * FROM user WHERE email = ?";
    private static final String LIST_USERS = "SELECT userId, name, surname, email, address, phoneNum, password, type, admin, specialization FROM User";
    private static final String UPDATE_USER = "UPDATE User SET name = ?, surname = ?, email = ?, address = ?, phoneNum = ?, password = ?, type = ?, admin = ?, specialization = ? WHERE userId = ?";
    private static final String DELETE_USER = "DELETE FROM User WHERE userId = ?";
    private static final String USER_BY_ID = "SELECT userId, name, surname, email, address, phoneNum, password, type, admin, specialization FROM User WHERE userId = ?";
    private static final String IS_USER_EXISTS = "SELECT userId, name, surname, email, address, phoneNum, password, type, admin FROM User WHERE email = ?";
    private static final String CREATE_USER = "INSERT INTO User(name, surname, email, address, phoneNum, password, type, admin, specialization) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String LIST_TEACHERS = "SELECT userId, name, surname, email, address, phoneNum, type, specialization FROM User WHERE type = ?";
    private static final String TEACHER_BY_ID = "SELECT userId, name, surname, email, address, phoneNum, type, specialization FROM User WHERE userId = ?";

    private static final String LIST_STUDENTS = "SELECT userId, name, surname, email, address, phoneNum, type FROM User WHERE type = ?";
    private static final String STUDENT_BY_ID = "SELECT userId, name, surname, email, address, phoneNum, type FROM User WHERE userId = ?";

    @Override
    public User getUserByEmail(String email) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(USER_BY_EMAIL, email);
             ResultSet rs = ps.executeQuery()) {
            User user;

            if (rs.first()) {
                user = createUser(rs);
            } else {
                user = null;
            }

            return user;
        } catch (SQLException e) {
            throw new DAOException("can not get user by id", e);
        }
    }

    @Override
    public List<User> findAll() throws DAOException {
        List<User> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_USERS);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                list.add(createUser(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of users", e);
        }
        return list;
    }

    @Override
    public User findById(int userId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(USER_BY_ID, valueOf(userId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createUser(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find user", e);
        }
    }

    private User createUser(ResultSet rs) throws SQLException {
        User user = new User();

        user.setUserId(rs.getInt("userId"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setEmail(rs.getString("email"));
        user.setAddress(rs.getString("address"));
        user.setPhoneNum(rs.getString("phoneNum"));
        user.setPassword(rs.getString("password"));
        if (rs.getString("type") != null) {
            user.setType(UserType.valueOf(rs.getString("type")));
        }
        if (rs.getString("specialization") != null) {
            user.setSpecialization(rs.getString("specialization"));
        } else {
            user.setSpecialization("not a teacher");
        }
        user.setAdmin(rs.getBoolean("admin"));

        return user;
    }

    @Override
    public boolean isExists(User entity) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(IS_USER_EXISTS, entity.getEmail());
             ResultSet rs = ps.executeQuery()) {
            return rs.first();
        } catch (SQLException e) {
            throw new DAOException("can not check is user exists", e);
        }
    }

    @Override
    public void add(User user) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(CREATE_USER)) {
            setParametersIntoPreparedStatement(ps, user);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not add user", e);
        }
    }

    @Override
    public void update(User user) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(UPDATE_USER)) {
            setParametersIntoPreparedStatement(ps, user);
            ps.setInt(10, user.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not update user", e);
        }
    }

    private void setParametersIntoPreparedStatement(PreparedStatement ps, User user) throws SQLException {
        ps.setString(1, user.getName());
        ps.setString(2, user.getSurname());
        ps.setString(3, user.getEmail());
        ps.setString(4, user.getAddress());
        ps.setString(5, user.getPhoneNum());
        ps.setString(6, user.getPassword());
        ps.setString(7, user.getType().name());
        ps.setBoolean(8, user.isAdmin());
        if (user.getSpecialization() != null) {
            ps.setString(9, user.getSpecialization());
        } else {
            ps.setString(9,"not a teacher");
        }
    }

    @Override
    public void delete(int userId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(DELETE_USER)) {
            ps.setInt(1, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("can not delete user", e);
        }
    }

    @Override
    public List<Teacher> findAllTeachers() throws DAOException {
        List<Teacher> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_TEACHERS, UserType.TEACHER.getType());
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                if (StringUtils.equalsIgnoreCase(rs.getString("type"), UserType.TEACHER.getType())) {
                    list.add(createTeacher(rs));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of teachers", e);
        }
        return list;
    }

    @Override
    public Teacher findTeacherById(int teacherId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(TEACHER_BY_ID, valueOf(teacherId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createTeacher(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find teacher", e);
        }
    }

    private Teacher createTeacher(ResultSet rs) throws SQLException {
        Teacher teacher = new Teacher();
        teacher.setUserId(rs.getInt("userId"));
        teacher.setName(rs.getString("name"));
        teacher.setSurname(rs.getString("surname"));
        teacher.setEmail(rs.getString("email"));
        teacher.setAddress(rs.getString("address"));
        teacher.setPhoneNum(rs.getString("phoneNum"));
        if (rs.getString("type") != null) {
            teacher.setType(UserType.valueOf(rs.getString("type")));
        }
        teacher.setSpecialization(rs.getString("specialization"));

        return teacher;
    }

    @Override
    public List<Student> findAllStudents() throws DAOException {
        List<Student> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_STUDENTS, UserType.STUDENT.getType());
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                if (StringUtils.equalsIgnoreCase(rs.getString("type"), UserType.STUDENT.getType())) {
                    list.add(createStudent(rs));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of students", e);
        }
        return list;
    }

    @Override
    public Student findStudentById(int studentId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(STUDENT_BY_ID, valueOf(studentId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createStudent(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find a student", e);
        }
    }

    private Student createStudent(ResultSet rs) throws SQLException {
        Student student = new Student();
        student.setUserId(rs.getInt("userId"));
        student.setName(rs.getString("name"));
        student.setSurname(rs.getString("surname"));
        student.setEmail(rs.getString("email"));
        student.setAddress(rs.getString("address"));
        student.setPhoneNum(rs.getString("phoneNum"));
        if (rs.getString("type") != null) {
            student.setType(UserType.valueOf(rs.getString("type")));
        }

        return student;
    }
}