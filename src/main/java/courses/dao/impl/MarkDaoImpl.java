package courses.dao.impl;

import static courses.dao.utils.DaoUtil.getPreparedStatement;

import static java.lang.String.valueOf;

import courses.dao.DAOException;
import courses.dao.GenericDao;
import courses.entities.Mark;
import courses.entities.Student;
import courses.entities.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MarkDaoImpl implements GenericDao<Mark> {

    private static final String LIST_MARKS = "SELECT markId, comment, mark, userId, taskId FROM Mark";
    private static final String UPDATE_MARK = "UPDATE Mark SET comment = ?, mark = ?, userId = ?, taskId = ? WHERE markId = ?";
    private static final String DELETE_MARK = "DELETE FROM Mark WHERE markId = ?";
    private static final String MARK_BY_ID = "SELECT markId, comment, mark, userId, taskId FROM Mark WHERE markId = ?";
    private static final String IS_MARK_EXISTS = "SELECT markId, comment, mark FROM Mark WHERE comment = ?";
    private static final String CREATE_MARK = "INSERT INTO Mark(comment, mark, userId, taskId) VALUES(?, ?, ?, ?)";

    private static final String GET_MARKS = "SELECT mark FROM Mark m INNER JOIN Task t WHERE m.taskId = t.taskId AND t.courseId = ?";

    public List<Mark> findMarksByCourseId(int courseId) throws DAOException {
        List<Mark> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(GET_MARKS, valueOf(courseId));
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Mark mark = new Mark();
                mark.setMark(rs.getInt("mark"));
                list.add(mark);
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of marks by courseId", e);
        }

        return list;
    }

    @Override
    public List<Mark> findAll() throws DAOException {
        List<Mark> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_MARKS);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                list.add(createMark(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of marks", e);
        }

        return list;
    }

    @Override
    public Mark findById(int markId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(MARK_BY_ID, valueOf(markId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createMark(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find marks", e);
        }
    }

    private Mark createMark(ResultSet rs) throws SQLException {
        Mark mark = new Mark();

        mark.setMarkId(rs.getInt("markId"));
        mark.setComment(rs.getString("comment"));
        mark.setMark(rs.getInt("mark"));
        mark.setStudent(new Student());
        mark.getStudent().setUserId(rs.getInt("userId"));
        mark.setTask(new Task());
        mark.getTask().setTaskId(rs.getInt("taskId"));

        return mark;
    }

    @Override
    public boolean isExists(Mark entity) throws DAOException {
        try (PreparedStatement ps = getPreparedStatement(IS_MARK_EXISTS, entity.getComment());
             ResultSet rs = ps.executeQuery()) {
            return rs.first();
        } catch (SQLException e) {
            throw new DAOException("can not check if mark exists", e);
        }
    }

    @Override
    public void add(Mark mark) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(CREATE_MARK)) {
            setParametersIntoPreparedStatement(ps, mark);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not add mark", e);
        }
    }

    @Override
    public void update(Mark mark) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(UPDATE_MARK)) {
            setParametersIntoPreparedStatement(ps, mark);
            ps.setInt(5, mark.getMarkId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not update mark", e);
        }
    }

    private void setParametersIntoPreparedStatement(PreparedStatement ps, Mark mark) throws SQLException {
        ps.setString(1, mark.getComment());
        ps.setInt(2, mark.getMark());
        ps.setInt(3, mark.getStudent().getUserId());
        ps.setInt(4, mark.getTask().getTaskId());
    }

    @Override
    public void delete(int markId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(DELETE_MARK)) {
            ps.setInt(1, markId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("can not delete mark", e);
        }
    }
}