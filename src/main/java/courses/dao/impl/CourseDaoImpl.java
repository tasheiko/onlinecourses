package courses.dao.impl;

import static courses.dao.utils.DaoUtil.getPreparedStatement;

import static java.lang.String.valueOf;

import courses.dao.DAOException;
import courses.dao.GenericDao;
import courses.entities.Course;
import courses.entities.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl implements GenericDao<Course> {

    private static final String LIST_COURSES = "SELECT courseId, type, title, numOfHours, description, userId FROM Course";
    private static final String UPDATE_COURSE = "UPDATE Course SET type = ?, title = ?, description = ?, numOfHours = ?, userId = ? WHERE courseId = ?";
    private static final String DELETE_COURSE = "DELETE FROM Course WHERE courseId = ?";
    private static final String COURSE_BY_ID = "SELECT courseId, type, title, numOfHours, description, userId FROM Course WHERE courseId = ?";
    private static final String IS_COURSE_EXISTS = "SELECT courseId, type, title, numOfHours, description FROM Course WHERE title = ?";
    private static final String CREATE_COURSE = "INSERT INTO Course(type, title, description, numOfHours, userId) VALUES(?, ?, ?, ?, ?)";

    @Override
    public List<Course> findAll() throws DAOException {
        List<Course> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_COURSES);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                list.add(createCourse(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of courses", e);
        }

        return list;
    }

    @Override
    public Course findById(int courseId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(COURSE_BY_ID, valueOf(courseId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createCourse(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find course", e);
        }
    }

    private Course createCourse(ResultSet rs) throws SQLException {
        Course course = new Course();

        course.setCourseId(rs.getInt("courseId"));
        course.setType(rs.getString("type"));
        course.setTitle(rs.getString("title"));
        course.setNumOfHours(rs.getInt("numOfHours"));
        course.setDescription(rs.getString("description"));
        course.setTeacher(new Teacher());
        course.getTeacher().setUserId(rs.getInt("userId"));

        return course;
    }

    @Override
    public boolean isExists(Course entity) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(IS_COURSE_EXISTS, entity.getTitle());
             ResultSet rs = ps.executeQuery()) {
            return rs.first();
        } catch (SQLException e) {
            throw new DAOException("can not check if course exists", e);
        }
    }

    @Override
    public void add(Course course) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(CREATE_COURSE)) {
            setParametersIntoPreparedStatement(ps, course);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not update course", e);
        }
    }

    @Override
    public void update(Course course) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(UPDATE_COURSE)) {
            setParametersIntoPreparedStatement(ps, course);
            ps.setInt(6, course.getCourseId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not update course", e);
        }
    }

    private void setParametersIntoPreparedStatement(PreparedStatement ps, Course course) throws SQLException {
        ps.setString(1, course.getType());
        ps.setString(2, course.getTitle());
        ps.setString(3, course.getDescription());
        ps.setInt(4, course.getNumOfHours());
        ps.setInt(5, course.getTeacher().getUserId());
    }

    @Override
    public void delete(int courseId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(DELETE_COURSE)) {
            ps.setInt(1, courseId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("can not delete course", e);
        }
    }
}