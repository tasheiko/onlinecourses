package courses.dao.impl;

import static courses.dao.utils.DaoUtil.getPreparedStatement;

import static java.lang.String.valueOf;

import courses.dao.DAOException;
import courses.dao.GenericDao;
import courses.entities.Course;
import courses.entities.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskDaoImpl implements GenericDao<Task> {

    private static final String LIST_TASKS = "SELECT taskId, nameOfTask, description, courseId FROM Task";
    private static final String UPDATE_TASK = "UPDATE Task SET nameOfTask = ?, description = ?, courseId = ? WHERE taskId = ?";
    private static final String DELETE_TASK = "DELETE FROM Task WHERE taskId = ?";

    private static final String TASK_BY_ID = "SELECT taskId, nameOfTask, description, courseId FROM Task WHERE taskId = ?";
    private static final String IS_TASK_EXISTS = "SELECT taskId, nameOfTask, description FROM Task WHERE nameOfTask = ?";
    private static final String CREATE_TASK = "INSERT INTO Task(nameOfTask, description, courseId) VALUES(?, ?, ?)";

    @Override
    public List<Task> findAll() throws DAOException {
        List<Task> list = new ArrayList<>();

        try (PreparedStatement ps = getPreparedStatement(LIST_TASKS);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                list.add(createTask(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Can not get a list of tasks", e);
        }
        return list;
    }

    @Override
    public Task findById(int taskId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(TASK_BY_ID, valueOf(taskId));
             ResultSet rs = ps.executeQuery()) {
            rs.first();

            return createTask(rs);
        } catch (SQLException e) {
            throw new DAOException("can not find task", e);
        }
    }

    private Task createTask(ResultSet rs) throws SQLException {
        Task task = new Task();

        task.setTaskId(rs.getInt("taskId"));
        task.setNameOfTask(rs.getString("nameOfTask"));
        task.setDescription(rs.getString("description"));
        task.setCourse(new Course());
        task.getCourse().setCourseId(rs.getInt("courseId"));

        return task;
    }

    @Override
    public boolean isExists(Task entity) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(IS_TASK_EXISTS, entity.getNameOfTask());
             ResultSet rs = ps.executeQuery()) {
            return rs.first();
        } catch (SQLException e) {
            throw new DAOException("can not check is task exists", e);
        }
    }

    @Override
    public void add(Task task) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(CREATE_TASK)) {
            setParametersIntoPreparedStatement(ps, task);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not add task", e);
        }
    }

    @Override
    public void update(Task task) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(UPDATE_TASK)) {
            setParametersIntoPreparedStatement(ps, task);
            ps.setInt(4, task.getTaskId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Can not update task", e);
        }
    }

    private void setParametersIntoPreparedStatement(PreparedStatement ps, Task task) throws SQLException {
        ps.setString(1, task.getNameOfTask());
        ps.setString(2, task.getDescription());
        ps.setInt(3, task.getCourse().getCourseId());
    }

    @Override
    public void delete(int taskId) throws DAOException {

        try (PreparedStatement ps = getPreparedStatement(DELETE_TASK)) {
            ps.setInt(1, taskId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("can not delete task", e);
        }
    }
}
