package courses.dao;

import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.User;

import java.util.List;

public interface UserDao extends GenericDao<User> {

    /**
     * Get entity by email from DB
     *
     * @param email - selected user email
     * @return user entity which matches
     * this email
     */
    User getUserByEmail(String email) throws DAOException;

    /**
     *Get entity by id from DB
     *
     * @param teacherId - selected teacher id
     * @return teacher entity which matches
     */
    Teacher findTeacherById(int teacherId) throws DAOException;

    /**
     *get all teacher entities from DB
     *
     * @return list of teachers
     */
    List<Teacher> findAllTeachers() throws DAOException;

    /**
     *Get entity by id from DB
     *
     * @param studentId - selected student id
     * @return student entity which matches
     */
    Student findStudentById(int studentId) throws DAOException;

    /**
     *get all student entities from DB
     *
     * @return list of students
     */
    List<Student> findAllStudents() throws DAOException;

}
