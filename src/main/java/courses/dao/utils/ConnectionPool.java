package courses.dao.utils;

import courses.dao.DAOException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static java.lang.Integer.parseInt;

public class ConnectionPool {

    private static List<Connection> connectionPool;
    private static List<Connection> usedConnections = new ArrayList<>();
    private static String user;
    private static String password;
    private static String url;
    private static String driver;
    private static int initialPoolSize;

    static {
        try (InputStream stream = ConnectionPool.class.getResourceAsStream("/config.properties")) {
            Properties properties = new Properties();
            properties.load(stream);
            user = properties.getProperty("db.user");
            password = properties.getProperty("db.password");
            url = properties.getProperty("db.url");
            driver = properties.getProperty("db.driver");
            initialPoolSize = parseInt(properties.getProperty("db.connectionPoolSize"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createConnectionPool() throws SQLException, ClassNotFoundException {
        List<Connection> pool = new ArrayList<>(initialPoolSize);

        for (int i = 0; i < initialPoolSize; i++) {
            pool.add(createConnection());
        }
        connectionPool = pool;
    }

    public static void closeConnectionPool() throws SQLException {
        for (Connection connection : connectionPool) {
            connection.close();
        }
        for (Connection connection : usedConnections) {
            connection.close();
        }
    }

    static Connection getConnection() throws DAOException {
        Connection connection;
        try {
            connection = connectionPool.remove(connectionPool.size() - 1);
        } catch (IndexOutOfBoundsException e) {
            throw new DAOException("you have reached max amount of connections", e);
        }
        usedConnections.add(connection);
        return connection;
    }

    static void releaseConnection(Connection connection) {
        connectionPool.add(0, connection);
        usedConnections.remove(connection);
    }

    private static Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName(driver);

        return DriverManager.getConnection(url, user, password);
    }
}
