package courses.dao.utils;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

class ConnectionManager {

    private static Map<String, Connection> openedConnections = new HashMap<>();

    static void push(Connection connection) {
        openedConnections.put(Thread.currentThread().getName(), connection);
    }

    static Connection pop() {
        return openedConnections.get(Thread.currentThread().getName());
    }

    static void remove() {
        openedConnections.remove(Thread.currentThread().getName());
    }
}