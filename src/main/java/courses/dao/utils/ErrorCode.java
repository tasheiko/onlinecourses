package courses.dao.utils;

public enum ErrorCode {
    /**
     * DAO error code that shows that was database connection error
     */
    DAO_EXCEPTION_CODE("error code 505"),
    /**
     * IO error code that shows that was Input/Output error
     */
    IO_EXCEPTION_CODE("error code 510");

    private String code;

    public String getCode() {
        return code;
    }

    ErrorCode(final String code) {
        this.code = code;
    }
}
