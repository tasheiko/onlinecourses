package courses.dao.utils;

import static java.lang.Integer.parseInt;

import courses.dao.DAOException;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class DaoUtil {

    Connection openConnection() throws DAOException {
        Connection connection = ConnectionPool.getConnection();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DAOException("can't set transaction auto commit", e);
        }
        return connection;
    }

    void closeConnection(Connection currentConnection) throws DAOException {
        if (currentConnection != null) {
            try {
                currentConnection.commit();
            } catch (SQLException e) {
                throw new DAOException("can't commit this transaction", e);
            }
            ConnectionPool.releaseConnection(currentConnection);
        }
    }

    void connectionRollback(Connection currentConnection) throws DAOException {
        if (currentConnection == null) {
            throw new DAOException("connection is null");
        }
        try {
            currentConnection.rollback();
        } catch (SQLException e) {
            throw new DAOException("can not rollback transaction", e);
        }
    }

    public static PreparedStatement getPreparedStatement(String sql) throws DAOException {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = ConnectionManager.pop().prepareStatement(sql);
        } catch (SQLException e) {
            throw new DAOException("preparedStatement was not created: ", e);
        }
        return preparedStatement;
    }

    public static PreparedStatement getPreparedStatement(String sql, String parameter) throws DAOException {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = ConnectionManager.pop().prepareStatement(sql);
            if (parameter != null) {
                if (StringUtils.isNumeric(parameter)) {
                    preparedStatement.setInt(1, parseInt(parameter));
                } else {
                    preparedStatement.setString(1, parameter);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("preparedStatement was not created: ", e);
        }
        return preparedStatement;
    }
}