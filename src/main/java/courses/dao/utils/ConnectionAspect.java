package courses.dao.utils;

import courses.dao.DAOException;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.AfterThrowing;

import java.sql.Connection;

@Aspect
public class ConnectionAspect {
    private DaoUtil dao = new DaoUtil();

    @Pointcut("execution(* courses.services.*.findAll(..)) " +
            "|| execution(* courses.services.*.getUserByEmail(..)) " +
            "|| execution(* courses.services.*.findById(..)) " +
            "|| execution(* courses.services.*.isExists(..)) " +
            "|| execution(* courses.services.*.delete(..)) " +
            "|| execution(* courses.services.*.update(..)) " +
            "|| execution(* courses.services.*.add(..)) " +
            "|| execution(* courses.services.*.findStudentById(..)) " +
            "|| execution(* courses.services.*.findAllStudents(..)) " +
            "|| execution(* courses.services.*.findTeacherById(..)) " +
            "|| execution(* courses.services.*.findAllTeachers(..)) " +
            "|| execution(* courses.services.*.calculateAverageMark(..)) ")
    protected void getAll() {
    }

    @Before("getAll()")
    public void getAllBefore() throws DAOException {
        Connection connection = dao.openConnection();
        ConnectionManager.push(connection);
    }

    @After("getAll()")
    public void getAllAfter() throws DAOException {
        dao.closeConnection(ConnectionManager.pop());
        ConnectionManager.remove();
    }

    @AfterThrowing(pointcut = "getAll()", throwing = "e")
    public void getAllAfterThrowing(Throwable e) throws DAOException {
        dao.connectionRollback(ConnectionManager.pop());
        throw new DAOException(e.getMessage(), e);
    }
}
