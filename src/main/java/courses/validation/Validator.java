package courses.validation;

import static org.apache.commons.lang3.StringUtils.isBlank;

import courses.entities.Course;
import courses.entities.Mark;
import courses.entities.Task;
import courses.entities.User;

import java.util.StringJoiner;

public class Validator {

    private static final String xssErrorMessage = "Please enter only letters, numbers, commas and spaces.";
    private static final String STRING_IS_BLANK = "Field(s) %s is(are) null or empty";
    private static final String allowedRegex = "[А-Яа-я\\w*\\s,.+()@#]*";

    public static String validateXss(String parameter) throws ValidateException {
        if (parameter != null) {
            if (!parameter.matches(allowedRegex)) {
                throw new ValidateException(xssErrorMessage);
            }
        }
        return parameter;
    }

    public static void validateCourse(Course course) throws ValidateException {
        StringJoiner invalidField = new StringJoiner(",");
        if (isBlank(course.getType())) {
            invalidField.add(" type");
        }
        if (course.getNumOfHours() == null) {
            invalidField.add(" Number of hours");
        }
        if (isBlank(course.getTitle())) {
            invalidField.add(" title");
        }
        if (isBlank(course.getDescription())) {
            invalidField.add(" description");
        }
        throwValidateException(invalidField.toString());
    }

    public static void validateMark(Mark mark) throws ValidateException {
        StringJoiner invalidField = new StringJoiner(",");
        if (isBlank(mark.getComment())) {
            invalidField.add(" comment");
        }
        if (mark.getMark() == null) {
            invalidField.add(" mark");
        }
        throwValidateException(invalidField.toString());
    }

    public static void validateTask(Task task) throws ValidateException {
        StringJoiner invalidField = new StringJoiner(",");
        if (isBlank(task.getNameOfTask())) {
            invalidField.add(" name of task");
        }
        if (isBlank(task.getDescription())) {
            invalidField.add(" description");
        }
        throwValidateException(invalidField.toString());
    }

    public static void validateUser(User user) throws ValidateException {
        StringJoiner invalidField = new StringJoiner(",");
        if (isBlank(user.getEmail())) {
            invalidField.add(" email");
        }
        if (isBlank(user.getPassword())) {
            invalidField.add(" password");
        }
        if (isBlank(user.getName())) {
            invalidField.add(" name");
        }
        if (isBlank(user.getSurname())) {
            invalidField.add(" surname");
        }
        if (isBlank(user.getAddress())) {
            invalidField.add(" address");
        }
        if (isBlank(user.getPhoneNum())) {
            invalidField.add(" phone number");
        }
        if (user.getType() != null && isBlank(user.getType().name())) {
            invalidField.add(" user type");
        }
        throwValidateException(invalidField.toString());
    }

    private static void throwValidateException(String invalidField) throws ValidateException {
        if (invalidField.length() > 1) {
            throw new ValidateException(String.format(STRING_IS_BLANK, invalidField));
        }
    }
}