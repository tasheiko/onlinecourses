DROP DATABASE `onlineCourses`;
CREATE SCHEMA IF NOT EXISTS `onlineCourses` DEFAULT CHARACTER SET utf8;
USE `onlineCourses`;

CREATE TABLE IF NOT EXISTS `onlineCourses`.`User` (
    `userId` INT NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(45),
    `password` VARCHAR(45),
    `name` VARCHAR(45) NOT NULL,
    `surname` VARCHAR(45) NOT NULL,
    `address` VARCHAR(45),
    `phoneNum` VARCHAR(45),
    `type` VARCHAR(45),
    `admin` TINYINT(1),
    `specialization` VARCHAR(45),
    PRIMARY KEY (`userId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Course` (
    `courseId` INT NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(45) NOT NULL,
    `title` VARCHAR(45) NOT NULL,
    `numOfHours` INT NOT NULL,
    `description` VARCHAR(90),
    `userId` INT NOT NULL,
    PRIMARY KEY (`courseId`),
    KEY `fk_teacher` (`userId`),
    CONSTRAINT `fk_teacher` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Task` (
    `taskId` INT NOT NULL AUTO_INCREMENT,
    `nameOfTask` VARCHAR(45) NOT NULL,
    `description` TEXT,
    `courseId` INT NOT NULL,
    PRIMARY KEY (`taskId`),
    KEY `fk_course` (`courseId`),
    CONSTRAINT `fk_course` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Mark` (
    `markId` INT NOT NULL AUTO_INCREMENT,
    `comment` VARCHAR(200) NOT NULL,
    `mark` INT NOT NULL,
    `userId` INT NOT NULL,
    `taskId` INT NOT NULL,
    PRIMARY KEY (`markId`),
    KEY `fk_student` (`userId`),
    CONSTRAINT `fk_student` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
    ON DELETE CASCADE,
    KEY `fk_task` (`taskId`),
    CONSTRAINT `fk_task` FOREIGN KEY (`taskId`) REFERENCES `task` (`taskId`)
    ON DELETE CASCADE
);

INSERT INTO `onlineCourses`.`User` (`email`, `password`, `name`, `surname`, `address`, `phoneNum`, `admin`)
VALUES ('yantasheiko@gmail.com', 'dager411', 'Yan', 'Tasheika', 'Minsk, st. Alexandrova, 11', '+375292091914', '1');
