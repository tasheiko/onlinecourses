package courses.converters;

import courses.entities.*;

class TestDataHelper {

    static Course getCourse() {
        Course course = new Course();
        course.setCourseId(1);
        course.setDescription("some description");
        course.setNumOfHours(11);
        course.setTitle("Java 9");
        course.setType("Programming");

        return course;
    }

    static Teacher getTeacherForCourse() {
        Teacher teacher = new Teacher();
        teacher.setName("Ivanka");
        teacher.setSurname("Grey");
        teacher.setUserId(1);

        return teacher;
    }

    static Mark getMark() {
        Mark mark = new Mark();
        mark.setMarkId(1);
        mark.setComment("some comment");
        mark.setMark(11);

        return mark;
    }

    static Student getStudentForMark() {
        Student student = new Student();
        student.setName("Jon");
        student.setSurname("Jones");

        return student;
    }

    static Task getTaskForMark() {
        Task task = new Task();
        task.setNameOfTask("some task");

        return task;
    }

    static User getUser() {
        User user = new User();
        user.setUserId(1);
        user.setEmail("gorka12@gmail.com");
        user.setName("Igor");
        user.setSurname("Bikov");
        user.setAddress("Gomel, st.Zavodskaya");
        user.setPhoneNum("+375291002099");
        user.setType(UserType.TEACHER);

        return user;
    }
}
