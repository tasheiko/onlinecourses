package courses.converters;

import static courses.converters.TestDataHelper.getUser;
import static courses.converters.UserConverter.toUserDto;

import courses.entities.User;
import courses.model.UserDto;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class UserConverterTest {

    @BeforeSuite
    public void setUp() {
    }

    @Test
    public void testToUserDto() {
        User user = getUser();
        UserDto dto = toUserDto(user);

        Assert.assertEquals(dto.getEmail(), user.getEmail());
        Assert.assertEquals(dto.getName(), user.getName());
        Assert.assertEquals(dto.getSurname(), user.getSurname());
        Assert.assertEquals(dto.getSpecialization(), user.getSpecialization());
    }
}