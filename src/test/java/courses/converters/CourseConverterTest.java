package courses.converters;

import static courses.converters.TestDataHelper.getCourse;
import static courses.converters.TestDataHelper.getTeacherForCourse;
import static courses.converters.CourseConverter.toCourseDto;

import courses.entities.Course;
import courses.entities.Teacher;
import courses.model.CourseDto;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class CourseConverterTest {

    @BeforeSuite
    public void setUp() {
    }

    @Test
    public void testToCourseDto() {
        Course course = getCourse();
        Teacher teacher = getTeacherForCourse();
        CourseDto dto = toCourseDto(course, teacher);

        Assert.assertEquals(dto.getCourseId(), course.getCourseId());
        Assert.assertEquals(dto.getType(), course.getType());
        Assert.assertEquals(dto.getTitle(), course.getTitle());
        Assert.assertEquals(dto.getName(), teacher.getName());
        Assert.assertEquals(dto.getSurname(), teacher.getSurname());
    }
}
