package courses.services;

import static courses.services.TestDataHelper.getCourse;
import static courses.services.TestDataHelper.getCoursesList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;

import courses.dao.DAOException;
import courses.dao.impl.CourseDaoImpl;
import courses.entities.Course;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceImplTest {

    @Mock
    private CourseDaoImpl dao;

    @InjectMocks
    private CourseServiceImpl courseService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAll_ReturnCourses() throws DAOException {
        List<Course> list = getCoursesList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(courseService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnCourse() throws DAOException {
        Course course = getCourse();
        when(dao.findById(1)).thenReturn(course);

        assertEquals(courseService.findById(1), course);
    }

    @Test
    public void testUpdate_ValidCourse_VerifyInvocationsNumber() throws DAOException {
        Course course = getCourse();
        when(dao.findById(1)).thenReturn(course);
        courseService.update(course);

        verify(dao, times(1)).update(course);
    }

    @Test
    public void testAdd_ValidCourse_VerifyInvocationsNumber() throws DAOException {
        Course course = getCourse();
        courseService.add(course);

        verify(dao, times(1)).add(course);
    }

    @Test
    public void testDelete_ValidCourse_InvocationsNumber() throws DAOException {
        Course course = getCourse();
        when(dao.findById(1)).thenReturn(course);
        courseService.delete(course.getCourseId());

        verify(dao, times(1)).delete(course.getCourseId());
    }
}