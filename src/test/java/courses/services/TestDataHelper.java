package courses.services;

import courses.entities.*;

import java.util.ArrayList;
import java.util.List;

class TestDataHelper {

    static List<Course> getCoursesList() {
        Course course = new Course();
        course.setCourseId(1);
        course.setType("Music");
        course.setTitle("Guitar playing");
        course.setNumOfHours(50);
        course.setDescription("some description");
        Course secondCourse = new Course();
        secondCourse.setType("Music");
        secondCourse.setTitle("Piano playing");
        secondCourse.setNumOfHours(120);
        secondCourse.setDescription("some description");

        List<Course> list = new ArrayList<>();
        list.add(course);
        list.add(secondCourse);
        return list;
    }

    static Course getCourse() {
        Course course = new Course();
        course.setCourseId(1);
        course.setType("Music");
        course.setTitle("Guitar playing");
        course.setNumOfHours(50);
        course.setDescription("some description");
        return course;
    }

    static List<Task> getTasksList() {
        Task task = new Task();
        task.setTaskId(1);
        task.setNameOfTask("guitar song");
        task.setDescription("send video with your song on teacher email");
        Task secondTask = new Task();
        secondTask.setTaskId(2);
        secondTask.setNameOfTask("HashMap and TreeMap");
        secondTask.setDescription("description of map task");

        List<Task> list = new ArrayList<>();
        list.add(task);
        list.add(secondTask);
        return list;
    }

    static Task getTask() {
        Task task = new Task();
        task.setTaskId(1);
        task.setNameOfTask("HashMap and TreeMap");
        task.setDescription("description of map task");
        return task;
    }

    static List<Mark> getMarksList() {
        Mark mark = new Mark();
        mark.setMarkId(1);
        mark.setComment("good job");
        mark.setMark(8);
        Mark secondMark = new Mark();
        secondMark.setMarkId(2);
        secondMark.setComment("good, but you have some little mistakes");
        secondMark.setMark(7);

        List<Mark> list = new ArrayList<>();
        list.add(mark);
        list.add(secondMark);
        return list;
    }

    static Mark getMark() {
        Mark mark = new Mark();
        mark.setMarkId(1);
        mark.setComment("good job");
        mark.setMark(8);
        return mark;
    }

    static List<User> getUsersList() {
        User firstUser = new User();
        firstUser.setUserId(1);
        firstUser.setEmail("immachine@gmail.com");
        firstUser.setPassword("hellomoto123");
        firstUser.setName("Evgeniy");
        firstUser.setSurname("Bolshoy");
        firstUser.setAddress("Gomel, st.Stepnaya 22");
        firstUser.setPhoneNum("+375442229080");
        firstUser.setType(UserType.valueOf("TEACHER"));
        User secondUser = new User();
        secondUser.setUserId(2);
        secondUser.setEmail("sobaka@gmail.com");
        secondUser.setPassword("hellr12333");
        secondUser.setName("Jack");
        secondUser.setSurname("Black");
        secondUser.setAddress("Grodno, st.Page 55");
        secondUser.setPhoneNum("+375443427080");
        secondUser.setType(UserType.valueOf("TEACHER"));
        User thirdUser = new User();
        thirdUser.setUserId(3);
        thirdUser.setEmail("glek@gmail.com");
        thirdUser.setPassword("disturb14");
        thirdUser.setName("John");
        thirdUser.setSurname("Frame");
        thirdUser.setAddress("Minsk, st.Bionika 15");
        thirdUser.setPhoneNum("+375442087765");
        thirdUser.setType(UserType.valueOf("STUDENT"));

        List<User> list = new ArrayList<>();
        list.add(firstUser);
        list.add(secondUser);
        list.add(thirdUser);
        return list;
    }

    static User getUser() {
        User user = new User();
        user.setUserId(3);
        user.setEmail("immachine@gmail.com");
        user.setPassword("hellomoto123");
        user.setName("Evgeniy");
        user.setSurname("Bolshoy");
        user.setAddress("Gomel, st.Stepnaya 22");
        user.setPhoneNum("+375442229080");
        user.setType(UserType.valueOf("TEACHER"));
        return user;
    }

    static List<Student> getStudentsList() {
        Student firstStudent = new Student();
        firstStudent.setUserId(1);
        firstStudent.setEmail("immachine@gmail.com");
        firstStudent.setName("Evgeniy");
        firstStudent.setSurname("Bolshoy");
        firstStudent.setAddress("Gomel, st.Stepnaya 22");
        firstStudent.setPhoneNum("+375442229080");
        Student secondStudent = new Student();
        secondStudent.setUserId(2);
        secondStudent.setEmail("sobaka@gmail.com");
        secondStudent.setName("Jack");
        secondStudent.setSurname("Black");
        secondStudent.setAddress("Grodno, st.Page 55");
        secondStudent.setPhoneNum("+375443427080");
        List<Student> list = new ArrayList<>();
        list.add(firstStudent);
        list.add(secondStudent);
        return list;
    }

    static Student getStudent() {
        Student student = new Student();
        student.setUserId(1);
        student.setEmail("immachine@gmail.com");
        student.setName("Evgeniy");
        student.setSurname("Bolshoy");
        student.setAddress("Gomel, st.Stepnaya 22");
        student.setPhoneNum("+375442229080");
        return student;
    }

    static List<Teacher> getTeachersList() {
        Teacher teacher = new Teacher();
        teacher.setUserId(1);
        teacher.setEmail("immachine@gmail.com");
        teacher.setName("Evgeniy");
        teacher.setSurname("Bolshoy");
        teacher.setAddress("Gomel, st.Stepnaya 22");
        teacher.setPhoneNum("+375442229080");
        Teacher secondTeacher = new Teacher();
        secondTeacher.setUserId(2);
        secondTeacher.setEmail("sobaka@gmail.com");
        secondTeacher.setName("Jack");
        secondTeacher.setSurname("Black");
        secondTeacher.setAddress("Grodno, st.Page 55");
        secondTeacher.setPhoneNum("+375443427080");

        List<Teacher> list = new ArrayList<>();
        list.add(teacher);
        list.add(secondTeacher);
        return list;
    }

    static Teacher getTeacher() {
        Teacher teacher = new Teacher();
        teacher.setUserId(1);
        teacher.setEmail("immachine@gmail.com");
        teacher.setName("Evgeniy");
        teacher.setSurname("Bolshoy");
        teacher.setAddress("Gomel, st.Stepnaya 22");
        teacher.setPhoneNum("+375442229080");
        teacher.setSpecialization("123123213213123132132131321321");
        return teacher;
    }
}
