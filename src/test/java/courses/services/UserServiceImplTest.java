package courses.services;

import static courses.services.TestDataHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import courses.dao.DAOException;
import courses.dao.impl.UserDaoImpl;
import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.User;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserDaoImpl dao;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAll_ReturnUsers() throws DAOException {
        List<User> list = getUsersList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(userService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnUser() throws DAOException {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);

        assertEquals(userService.findById(3), user);
    }

    @Test
    public void testUpdate_ValidUser_VerifyInvocationsNumber() throws DAOException {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);
        userService.update(user);

        verify(dao, times(1)).update(user);
    }

    @Test
    public void testAdd_ValidUser_VerifyInvocationsNumber() throws DAOException {
        User user = getUser();
        userService.add(user);

        verify(dao, times(1)).add(user);
    }

    @Test
    public void testDelete_ValidUser_VerifyInvocationsNumber() throws DAOException {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);
        userService.delete(user.getUserId());

        verify(dao, times(1)).delete(user.getUserId());
    }

    @Test
    public void testIsExists_ValidUser_ReturnCondition() throws DAOException {
        when(dao.isExists(any())).thenReturn(true);

        assertTrue(userService.isExists(any()));
    }

    @Test
    public void testGetUserByEmail_ValidUserEmail_ReturnUser() throws DAOException {
        User user = getUser();
        when(dao.getUserByEmail(user.getEmail())).thenReturn(user);

        assertEquals(userService.getUserByEmail(user.getEmail()), user);
    }

    @Test
    public void testFindAllStudents_ReturnStudents() throws DAOException {
        List<Student> list = getStudentsList();
        when(dao.findAllStudents()).thenReturn(list);

        assertEquals(userService.findAllStudents(), list);
    }

    @Test
    public void testFindStudentById_ValidId_ReturnStudent() throws DAOException {
        Student student = getStudent();
        when(dao.findStudentById(1)).thenReturn(student);

        assertEquals(userService.findStudentById(1), student);
    }

    @Test
    public void testFindAllTeachers_ReturnTeachers() throws DAOException {
        List<Teacher> list = getTeachersList();
        when(dao.findAllTeachers()).thenReturn(list);

        assertEquals(userService.findAllTeachers(), list);
    }

    @Test
    public void testFindTeacherById_ValidId_ReturnTeacher() throws DAOException {
        Teacher teacher = getTeacher();
        when(dao.findTeacherById(1)).thenReturn(teacher);

        assertEquals(userService.findTeacherById(1), teacher);
    }
}
