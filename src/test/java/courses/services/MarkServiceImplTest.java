package courses.services;

import static courses.services.TestDataHelper.getMark;
import static courses.services.TestDataHelper.getMarksList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;

import courses.dao.DAOException;
import courses.dao.impl.MarkDaoImpl;
import courses.entities.Mark;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class MarkServiceImplTest {

    @Mock
    private MarkDaoImpl dao;

    @InjectMocks
    private MarkServiceImpl markService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAll_ReturnMarks() throws DAOException {
        List<Mark> list = getMarksList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(markService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnMark() throws DAOException {
        Mark mark = getMark();
        when(dao.findById(1)).thenReturn(mark);

        assertEquals(markService.findById(1), mark);
    }

    @Test
    public void testUpdate_ValidMark_VerifyInvocationsNumber() throws DAOException {
        Mark mark = getMark();
        when(dao.findById(1)).thenReturn(mark);
        markService.update(mark);

        verify(dao, times(1)).update(mark);
    }

    @Test
    public void testAdd_ValidMark_VerifyInvocationsNumber() throws DAOException {
        Mark mark = getMark();
        markService.add(mark);

        verify(dao, times(1)).add(mark);
    }

    @Test
    public void testDelete_ValidMark_VerifyInvocationsNumber() throws DAOException {
        int markId = 1;
        markService.delete(markId);

        verify(dao, times(1)).delete(markId);
    }
}