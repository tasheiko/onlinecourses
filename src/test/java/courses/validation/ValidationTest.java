package courses.validation;

import courses.entities.Course;

import courses.entities.Mark;
import courses.entities.Task;
import courses.entities.User;

import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class ValidationTest {

    @BeforeSuite
    public void setUp() {
    }

    @Test(expectedExceptions = ValidateException.class)
    public void testValidateCourse_InvalidCourse_ThrowException() throws Exception {
        Course course = new Course();
        Validator.validateCourse(course);
    }

    @Test(expectedExceptions = ValidateException.class)
    public void testValidateMark_InvalidMark_ThrowException() throws Exception {
        Mark mark = new Mark();
        Validator.validateMark(mark);
    }

    @Test(expectedExceptions = ValidateException.class)
    public void testValidateTask_InvalidTask_ThrowException() throws Exception {
        Task task = new Task();
        Validator.validateTask(task);
    }

    @Test(expectedExceptions = ValidateException.class)
    public void testValidateUser_InvalidUser_ThrowException() throws Exception {
        User user = new User();
        Validator.validateUser(user);
    }

    @Test(expectedExceptions = ValidateException.class)
    public void testValidateXss_invalidString_ThrowException() throws Exception {
        String parameter = "John<alert>xss</alert>";
        Validator.validateXss(parameter);
    }
}